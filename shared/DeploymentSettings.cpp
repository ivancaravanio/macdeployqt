/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "DeploymentSettings.h"

#include "DeploymentSettingsParser.h"

#include <QDir>

#include <algorithm>

using namespace MacDeployQt::Shared;

const VerbosityLevel DeploymentSettings::s_logVerbosityLevelDefault = VerbosityLevelWarnings;
const bool           DeploymentSettings::s_shouldDeployPluginsDefault = true;
const bool           DeploymentSettings::s_shouldCreateDmgDefault = false;
const bool           DeploymentSettings::s_shouldUseDebugLibsDefault = false;
const bool           DeploymentSettings::s_shouldRunStripDefault = true;
const QString        DeploymentSettings::s_qmlFilesDirPathDefault;
const bool           DeploymentSettings::s_alwaysOverwriteDefault = false;
const QString        DeploymentSettings::s_codeSignIdentityDefault;
const bool           DeploymentSettings::s_isAppStoreCompliantDefault = false;
const QStringList    DeploymentSettings::s_additionalLibrarySearchDirPathsDefault;

DeploymentSettings::DeploymentSettings()
    : m_logVerbosityLevel{ VerbosityLevelNone }
    , m_shouldDeployPlugins{ false }
    , m_shouldCreateDmg{ false }
    , m_shouldUseDebugLibs{ false }
    , m_shouldRunStrip{ false }
    , m_alwaysOverwrite{ false }
    , m_isAppStoreCompliant{ false }
{
}

DeploymentSettings::DeploymentSettings(const ModuleInfo& aDeploymentTarget)
    : m_logVerbosityLevel{ s_logVerbosityLevelDefault }
    , m_shouldDeployPlugins{ s_shouldDeployPluginsDefault }
    , m_shouldCreateDmg{ s_shouldCreateDmgDefault }
    , m_shouldUseDebugLibs{ s_shouldUseDebugLibsDefault }
    , m_shouldRunStrip{ s_shouldRunStripDefault }
    , m_qmlFilesDirPath{ s_qmlFilesDirPathDefault }
    , m_alwaysOverwrite{ s_alwaysOverwriteDefault }
    , m_codeSignIdentity{ s_codeSignIdentityDefault }
    , m_isAppStoreCompliant{ s_isAppStoreCompliantDefault }
    , m_additionalLibrarySearchDirPaths{ s_additionalLibrarySearchDirPathsDefault }
{
    this->setDeploymentTarget( aDeploymentTarget );
}

DeploymentSettings::DeploymentSettings(const QCoreApplication& aApp)
    : m_logVerbosityLevel{ VerbosityLevelNone }
    , m_shouldDeployPlugins{ false }
    , m_shouldCreateDmg{ false }
    , m_shouldUseDebugLibs{ false }
    , m_shouldRunStrip{ false }
    , m_alwaysOverwrite{ false }
    , m_isAppStoreCompliant{ false }
{
    this->load( aApp );
}

DeploymentSettings::DeploymentSettings(const QStringList& aArguments)
    : m_logVerbosityLevel{ VerbosityLevelNone }
    , m_shouldDeployPlugins{ false }
    , m_shouldCreateDmg{ false }
    , m_shouldUseDebugLibs{ false }
    , m_shouldRunStrip{ false }
    , m_alwaysOverwrite{ false }
    , m_isAppStoreCompliant{ false }
{
    this->load( aArguments );
}

void DeploymentSettings::setDeploymentTarget(const ModuleInfo& aDeploymentTarget)
{
    if (aDeploymentTarget.isValid())
    {
        m_deploymentTarget = aDeploymentTarget;
    }
}

const ModuleInfo& DeploymentSettings::deploymentTarget() const
{
    return m_deploymentTarget;
}

void DeploymentSettings::setLogVerbosityLevel(const VerbosityLevel aLogVerbosityLevel)
{
    if ( Logger::isVerbosityLevelValid( aLogVerbosityLevel ) )
    {
        m_logVerbosityLevel = aLogVerbosityLevel;
    }
}

VerbosityLevel DeploymentSettings::logVerbosityLevel() const
{
    return m_logVerbosityLevel;
}

void DeploymentSettings::setShouldDeployPlugins(const bool aShouldDeployPlugins)
{
    m_shouldDeployPlugins = aShouldDeployPlugins;
}

bool DeploymentSettings::shouldDeployPlugins() const
{
    return m_shouldDeployPlugins;
}

void DeploymentSettings::setShouldCreateDmg(const bool aShouldCreateDmg)
{
    m_shouldCreateDmg = aShouldCreateDmg;
}

bool DeploymentSettings::shouldCreateDmg() const
{
    return m_shouldCreateDmg;
}

void DeploymentSettings::setUseDebugLibs(const bool aShouldUseDebugLibs)
{
    m_shouldUseDebugLibs = aShouldUseDebugLibs;
}

bool DeploymentSettings::shouldUseDebugLibs() const
{
    return m_shouldUseDebugLibs;
}

void DeploymentSettings::setShouldRunStrip(const bool aShouldRunStrip)
{
    m_shouldRunStrip = aShouldRunStrip;
}

bool DeploymentSettings::shouldRunStrip() const
{
    return m_shouldRunStrip;
}

void DeploymentSettings::setAdditionalExecutables(const QList<ModuleInfo>& aAdditionalExecutables)
{
    if ( std::any_of( aAdditionalExecutables.constBegin(),
                      aAdditionalExecutables.constEnd(),
                      [](const ModuleInfo& aModuleInfo){ return ! aModuleInfo.isValid(); } ) )
    {
        return;
    }

    m_additionalExecutables = aAdditionalExecutables;
}

QList<ModuleInfo> DeploymentSettings::additionalExecutables() const
{
    return m_additionalExecutables;
}

void DeploymentSettings::setQmlFilesDirPath(const QString& aQmlFilesDirPath)
{
    if (DeploymentSettings::isQmlFilesDirPathValid(aQmlFilesDirPath))
    {
        m_qmlFilesDirPath = aQmlFilesDirPath;
    }
}

QString DeploymentSettings::qmlFilesDirPath() const
{
    return m_qmlFilesDirPath;
}

bool DeploymentSettings::isQmlFilesDirPathValid(const QString& aQmlFilesDirPath)
{
    return DeploymentSettings::isDirPathIsEmptyOrExists(aQmlFilesDirPath);
}

void DeploymentSettings::setAlwaysOverwrite(const bool aAlwaysOverwrite)
{
    m_alwaysOverwrite = aAlwaysOverwrite;
}

bool DeploymentSettings::alwaysOverwrite() const
{
    return m_alwaysOverwrite;
}

void DeploymentSettings::setCodeSignIdentity(const QString& aCodeSignIdentity)
{
    m_codeSignIdentity = aCodeSignIdentity;
}

QString DeploymentSettings::codeSignIdentity() const
{
    return m_codeSignIdentity;
}

bool DeploymentSettings::shouldCodeSign() const
{
    return ! m_codeSignIdentity.isEmpty();
}

void DeploymentSettings::setIsAppStoreCompliant(const bool aIsAppStoreCompliant)
{
    m_isAppStoreCompliant = aIsAppStoreCompliant;
}

bool DeploymentSettings::isAppStoreCompliant() const
{
    return m_isAppStoreCompliant;
}

void DeploymentSettings::setAdditionalLibrarySearchDirPaths(const QStringList& aAdditionalLibrarySearchDirPaths)
{
    if ( std::any_of( aAdditionalLibrarySearchDirPaths.constBegin(),
                      aAdditionalLibrarySearchDirPaths.constEnd(),
                      [](const QString& aDirPath){ return ! DeploymentSettings::isDirPathIsEmptyOrExists(aDirPath); } ) )
    {
        return;
    }

    m_additionalLibrarySearchDirPaths = aAdditionalLibrarySearchDirPaths;
}

QStringList DeploymentSettings::additionalLibrarySearchDirPaths() const
{
    return m_additionalLibrarySearchDirPaths;
}

bool DeploymentSettings::load( const QCoreApplication& aApp, QString* const aErrorText )
{
    *this = DeploymentSettingsParser::deploymentSettings( aApp, aErrorText );
    return this->isValid();
}

bool DeploymentSettings::load( const QStringList& aArguments, QString* const aErrorText )
{
    *this = DeploymentSettingsParser::deploymentSettings( aArguments, aErrorText );
    return this->isValid();
}

bool DeploymentSettings::isValid() const
{
    return m_deploymentTarget.isValid();
}

bool DeploymentSettings::isDirPathIsEmptyOrExists(const QString& aDirPath)
{
    return aDirPath.isEmpty() || QDir(aDirPath).exists();
}
