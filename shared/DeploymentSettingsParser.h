/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGSPARSER_H
#define MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGSPARSER_H

#include "DeploymentSettings.h"

#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QtGlobal>

QT_FORWARD_DECLARE_CLASS(QCommandLineParser)

namespace MacDeployQt {
namespace Shared {

class DeploymentSettingsParser
{
public:
    DeploymentSettingsParser() Q_DECL_EQ_DELETE;

    static DeploymentSettings deploymentSettings( const QStringList& aArguments,
                                                  QString* const aErrorText = nullptr );
    static DeploymentSettings deploymentSettings( const QCoreApplication& aApp,
                                                  QString* const aErrorText = nullptr );

private:
    enum OptionArgumentType
    {
        OptionArgumentTypeUnknown                         = -1,
        OptionArgumentTypeNoPlugins                       =  0,
        OptionArgumentTypeDMG                             =  1,
        OptionArgumentTypeNoStrip                         =  2,
        OptionArgumentTypeUseDebugLibs                    =  3,
        OptionArgumentTypeLogVerbosity                    =  4,
        OptionArgumentTypeExecutable                      =  5,
        OptionArgumentTypeQmlDir                          =  6,
        OptionArgumentTypeAlwaysOverwrite                 =  7,
        OptionArgumentTypeCodeSignIdentity                =  8,
        OptionArgumentTypeIsAppstoreCompliant             =  9,
        OptionArgumentTypeAdditionalLibrarySearchDirPaths = 10,
        OptionArgumentTypesCount                          = 11
    };

private:
    static QCommandLineParser* parser();
    static QString appDescription();
    static QString optionArgumentName( const OptionArgumentType aOptionArgumentType, const bool aShouldDecorate = false );
    static OptionArgumentType optionArgumentType( const QString& aOptionArgumentName );
    static QString optionArgumentValueName( const OptionArgumentType aOptionArgumentType, const bool aShouldDecorate = false );
    static QString optionArgumentDescription( const OptionArgumentType aOptionArgumentType );
    static bool isOptionArgumentTypeValid( const OptionArgumentType aOptionArgumentType );

    static DeploymentSettings deploymentSettings( QCommandLineParser* const aParser,
                                                  QString* const errorText = nullptr );
    static QString errorText( QCommandLineParser* const aParser,
                              const QString& aSpecificErrorMessage = QString() );
};

}
}

#endif // MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGSPARSER_H
