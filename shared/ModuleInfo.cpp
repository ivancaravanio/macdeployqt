/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "moduleinfo.h"

#include <QDebug>
#include <QDirIterator>
#include <QFileInfo>
#include <QProcess>
#include <QStringBuilder>

#include <mach-o/loader.h>

#ifdef Q_OS_DARWIN
    #include <CoreFoundation/CoreFoundation.h>
#endif

using namespace MacDeployQt::Shared;

const QString ModuleInfo::contentsDirName       = "Contents";
const QString ModuleInfo::resourcesDirName      = "Resources";
const QString ModuleInfo::frameworksDirName     = "Frameworks";
const QString ModuleInfo::librariesDirName      = "Libraries";
const QString ModuleInfo::pluginsDirName        = "PlugIns";
const QString ModuleInfo::versionsDirName       = "Versions";
const QString ModuleInfo::currentVersionDirName = "Current";
const QString ModuleInfo::helpersDirName        = "Helpers";

const QString ModuleInfo::exePath               = "@executable_path";
const QString ModuleInfo::loaderPath            = "@loader_path";
const QString ModuleInfo::rPath                 = "@rpath";

const QString ModuleInfo::informationPropertyListFileName = "Info.plist";

ModuleInfo::ModuleInfo()
    : m_type(ModuleTypeUnknown)
{
}

ModuleInfo::ModuleInfo(const QString &modulePath)
    : m_type(ModuleTypeUnknown)
{
    QFileInfo moduleInfo(modulePath);
    if (!moduleInfo.exists())
        return;

    if (moduleInfo.isSymLink()) {
        moduleInfo.setFile(moduleInfo.symLinkTarget());
        if (!moduleInfo.exists())
            return;
    }

    if (moduleInfo.isDir()
        && moduleInfo.isBundle()) {
        const QString moduleExt = moduleInfo.suffix();
        if (moduleExt == ModuleInfo::moduleExtension(ModuleTypeApplicationBundle)) {
            const QString appBundlePath = moduleInfo.canonicalFilePath();
            const QChar dirSeparator = QDir::separator();
            static const QString appBundleMacOSRelativeDirPath = contentsDirName
                                                                 % dirSeparator
                                                                 % "MacOS";

            const QFileInfo executableFileInfo(
                        appBundlePath
                        % dirSeparator
                        % appBundleMacOSRelativeDirPath
                        % dirSeparator
                        % moduleInfo.completeBaseName());
            if (!executableFileInfo.exists())
                return;

            const QString executableFilePath = executableFileInfo.canonicalFilePath();
            const int fileType = ModuleInfo::machoHeaderFileType(executableFilePath);
            if (fileType != MH_EXECUTE)
                return;

            if (!ModuleInfo::verifyInformationPropertyList(
                        appBundlePath
                        % dirSeparator
                        % contentsDirName
                        % dirSeparator
                        % informationPropertyListFileName,
                        BundleTypeApplication,
                        executableFileInfo.fileName())) {
                return;
            }

            m_type           = ModuleTypeApplicationBundle;
            m_binaryFilePath = executableFilePath;
            m_path           = appBundlePath;
            findBundleLibraryFilePaths(m_path);
        } else if (moduleExt == ModuleInfo::moduleExtension(ModuleTypeLibraryBundle)) {
            const QString libBundlePath = moduleInfo.canonicalFilePath();
            QFileInfo libraryBinaryFileInfo(
                        QDir(libBundlePath),
                        moduleInfo.completeBaseName());
            if (!libraryBinaryFileInfo.exists())
                return;

            if (!libraryBinaryFileInfo.isSymLink())
                return;

            libraryBinaryFileInfo.setFile(libraryBinaryFileInfo.symLinkTarget());
            if (!libraryBinaryFileInfo.exists())
                return;

            const QString libraryBinaryFilePath = libraryBinaryFileInfo.canonicalFilePath();
            const int fileType = ModuleInfo::machoHeaderFileType(libraryBinaryFilePath);
            if (fileType != MH_DYLIB)
                return;

            const QChar dirSeparator = QDir::separator();
            QString plistFilePath = libraryBinaryFileInfo.absolutePath()
                                    % dirSeparator
                                    % resourcesDirName
                                    % dirSeparator
                                    % informationPropertyListFileName;
            if (!QFile::exists(plistFilePath)) {
                plistFilePath = libBundlePath
                                % dirSeparator
                                % contentsDirName
                                % dirSeparator
                                % informationPropertyListFileName;
                if (!QFile::exists(plistFilePath))
                    return;
            }

            if (!ModuleInfo::verifyInformationPropertyList(plistFilePath,
                                                           BundleTypeLibrary,
                                                           libraryBinaryFileInfo.fileName())) {
                return;
            }

            m_type           = ModuleTypeLibraryBundle;
            m_binaryFilePath = libraryBinaryFilePath;
            m_path           = libBundlePath;
            findBundleLibraryFilePaths(m_path);
        }
    } else if (moduleInfo.isFile()) {
        // const QString moduleExt = moduleInfo.suffix();
        const int fileType = ModuleInfo::machoHeaderFileType(moduleInfo.canonicalFilePath());
        if (fileType == MH_EXECUTE
            /*&& moduleExt == ModuleInfo::moduleExtension(ModuleTypeApplicationExecutable)*/) {
            m_type = ModuleTypeApplicationExecutable;
            m_path = m_binaryFilePath = moduleInfo.canonicalFilePath();
        } else if (fileType == MH_DYLIB
                   /*&& moduleExt == ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)*/) {
            m_type = ModuleTypeLibraryDynamicBinary;
            m_path = m_binaryFilePath = moduleInfo.canonicalFilePath();
        } else if (fileType == MH_OBJECT
                   /*&& moduleExt == ModuleInfo::moduleExtension(ModuleTypeLibraryStaticBinary)*/) {
            m_type = ModuleTypeLibraryStaticBinary;
            m_path = m_binaryFilePath = moduleInfo.canonicalFilePath();
        }
    }
}

ModuleInfo::ModuleInfo(const ModuleInfo &moduleInfo)
    : m_type(moduleInfo.m_type),
      m_binaryFilePath(moduleInfo.m_binaryFilePath),
      m_path(moduleInfo.m_path),
      m_bundleLibraryModules(moduleInfo.m_bundleLibraryModules)
{
}

ModuleInfo::~ModuleInfo()
{
}

bool ModuleInfo::isValid() const
{
    return isBundle()
           || isBinary();
}

ModuleType ModuleInfo::type() const
{
    return m_type;
}

bool ModuleInfo::isBundle() const
{
    return m_type == ModuleTypeApplicationBundle
           || m_type == ModuleTypeLibraryBundle;
}

bool ModuleInfo::isBinary() const
{
    return m_type == ModuleTypeApplicationExecutable
           || m_type == ModuleTypeLibraryDynamicBinary
           || m_type == ModuleTypeLibraryStaticBinary;
}

bool ModuleInfo::isApplication() const
{
    return m_type == ModuleTypeApplicationBundle
           || m_type == ModuleTypeApplicationExecutable;
}

bool ModuleInfo::isLibrary() const
{
    return m_type == ModuleTypeLibraryBundle
           || m_type == ModuleTypeLibraryDynamicBinary
           || m_type == ModuleTypeLibraryStaticBinary;
}

const QString &ModuleInfo::path() const
{
    return m_path;
}

const QString &ModuleInfo::binaryFilePath() const
{
    return m_binaryFilePath;
}

QString ModuleInfo::calculatedBinaryFilePath() const
{
    QString binaryFilePath;
#ifdef Q_OS_DARWIN
    CFStringRef bundlePath = m_path.toCFString();
    CFURLRef bundleURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, bundlePath,
                                                       kCFURLPOSIXPathStyle, true);
    CFRelease(bundlePath);
    CFBundleRef bundle = CFBundleCreate(kCFAllocatorDefault, bundleURL);
    if (bundle) {
        CFURLRef executableURL = CFBundleCopyExecutableURL(bundle);
        if (executableURL) {
            CFURLRef absoluteExecutableURL = CFURLCopyAbsoluteURL(executableURL);
            if (absoluteExecutableURL) {
                CFStringRef executablePath = CFURLCopyFileSystemPath(absoluteExecutableURL,
                                                                     kCFURLPOSIXPathStyle);
                if (executablePath) {
                    binaryFilePath = QString::fromCFString(executablePath);
                    CFRelease(executablePath);
                }
                CFRelease(absoluteExecutableURL);
            }
            CFRelease(executableURL);
        }
        CFRelease(bundle);
    }
    CFRelease(bundleURL);
#endif

    return binaryFilePath;
}

QString ModuleInfo::workingDirPath() const
{
    if (isBundle())
        return m_path;

    if (isBinary())
        return QFileInfo(m_binaryFilePath).canonicalPath();

    return QString();
}

QString ModuleInfo::bundleRelativeBinaryFilePath() const
{
    if (!isValid())
        return QString();

    return bundleRelativeBinaryDirPath()
           + QDir::separator()
           + QFileInfo(m_binaryFilePath).fileName();
}

QString ModuleInfo::bundleRelativeBinaryDirPath() const
{
    if (isBundle())
        return QFileInfo(QDir(m_path).relativeFilePath(m_binaryFilePath)).path();

    if (isBinary())
        return QChar(QLatin1Char('.'));

    return QString();
}

QString ModuleInfo::dependenciesRelativeDirPath() const
{
    switch (m_type) {
    case ModuleTypeApplicationBundle:
        return ModuleInfo::contentsDirName;
    case ModuleTypeLibraryBundle:
        // fall through
    case ModuleTypeApplicationExecutable:
        // fall through
    case ModuleTypeLibraryDynamicBinary:
        // fall through
    case ModuleTypeLibraryStaticBinary:
        return bundleRelativeBinaryDirPath();
    default:
        break;
    }

    return QString();
}

QString ModuleInfo::dependenciesAbsoluteDirPath() const
{
    if (!isValid())
        return QString();

    return workingDirPath()
           % QDir::separator()
           % dependenciesRelativeDirPath();
}

QString ModuleInfo::librariesInstallNameDirPath() const
{
    switch (m_type) {
    case ModuleTypeApplicationBundle: {
        const QChar dirSeparator = QDir::separator();
        static const QString exeRelativeDependenciesDirPath = exePath
                                                              % dirSeparator
                                                              % ".."
                                                              % dirSeparator
                                                              % frameworksDirName;
        return exeRelativeDependenciesDirPath;

    }
    case ModuleTypeApplicationExecutable: {
        static const QString exeRelativeDependenciesDirPath = exePath
                                                              % QDir::separator()
                                                              % frameworksDirName;
        return exeRelativeDependenciesDirPath;
    }

    case ModuleTypeLibraryBundle:
        // fall through
    case ModuleTypeLibraryDynamicBinary:
        // fall through
    case ModuleTypeLibraryStaticBinary: {
        static const QString libRelativeDependenciesDirPath = loaderPath
                                                              % QDir::separator()
                                                              % librariesDirName;
        return libRelativeDependenciesDirPath;
    }
    default:
        break;
    }

    return QString();
}

QString ModuleInfo::librariesRelativeDirPath() const
{
    QString path = dependenciesRelativeDirPath();
    switch (m_type) {
    case ModuleTypeApplicationBundle:
        // fall through
    case ModuleTypeApplicationExecutable:
        path += QDir::separator()
                + ModuleInfo::frameworksDirName;
        break;
    case ModuleTypeLibraryBundle:
        // fall through
    case ModuleTypeLibraryDynamicBinary:
        // fall through
    case ModuleTypeLibraryStaticBinary:
        path += QDir::separator()
                + ModuleInfo::librariesDirName;
        break;
    default:
        break;
    }

    return path;
}

QString ModuleInfo::librariesAbsoluteDirPath() const
{
    if (!isValid())
        return QString();

    return QDir( workingDirPath()
                 % QDir::separator()
                 % librariesRelativeDirPath() ).canonicalPath();
}

QString ModuleInfo::plugInsRelativeDirPath() const
{
    if (!isValid())
        return QString();

    return dependenciesRelativeDirPath()
           % QDir::separator()
           % ModuleInfo::pluginsDirName;
}

QString ModuleInfo::plugInsAbsoluteDirPath() const
{
    if (!isValid())
        return QString();

    return workingDirPath()
           % QDir::separator()
           % plugInsRelativeDirPath();
}

QString ModuleInfo::resourcesRelativeDirPath() const
{
    if (!isValid())
        return QString();

    return dependenciesRelativeDirPath()
           % QDir::separator()
           % ModuleInfo::resourcesDirName;
}

QString ModuleInfo::resourcesAbsoluteDirPath() const
{
    if (!isValid())
        return QString();

    return workingDirPath()
           % QDir::separator()
           % resourcesRelativeDirPath();
}

QStringList ModuleInfo::librariesFilePaths( const bool aShouldCollectNamesOnly ) const
{
    if ( ! this->isValid() )
    {
        return QStringList{};
    }

    QStringList paths;
    QDirIterator iter{this->path(),
                      QStringList{} << ( "*." + ModuleInfo::moduleExtension( ModuleTypeLibraryDynamicBinary ) ),
                      QDir::Files, QDirIterator::Subdirectories};

    while (iter.hasNext()) {
        iter.next();

        const QFileInfo fileInfo = iter.fileInfo();
        paths << ( aShouldCollectNamesOnly
                   ? fileInfo.fileName()
                   : fileInfo.filePath() );
    }

    return paths;
}

QStringList ModuleInfo::frameworksPaths( const bool aShouldCollectNamesOnly ) const
{
    if ( ! this->isValid() )
    {
        return QStringList{};
    }

    QStringList paths;
    QDirIterator frameworksDirIter{
                this->librariesAbsoluteDirPath(),
                QStringList{} << ( "*." + ModuleInfo::moduleExtension( ModuleTypeLibraryBundle ) ),
                QDir::Dirs};
    while (frameworksDirIter.hasNext()) {
        frameworksDirIter.next();

        const QFileInfo fileInfo = frameworksDirIter.fileInfo();
        paths << ( aShouldCollectNamesOnly
                   ? fileInfo.fileName()
                   : fileInfo.filePath() );
    }

    return paths;
}

const QList<ModuleInfo> &ModuleInfo::bundleLibraryModules() const
{
    return m_bundleLibraryModules;
}

const QString &ModuleInfo::moduleExtension(const ModuleType moduleType)
{
    static const QString unknownExtension;
    static const QString applicationBundleDirectoryExtension = "app";
    static const QString applicationExecutableFileExtension;
    static const QString libraryBundleDirectoryExtension     = "framework";
    static const QString dynamicLibraryBinaryFileExtension   = "dylib";
    static const QString staticLibraryBinaryFileExtension    = QChar(QLatin1Char('a'));

    switch (moduleType) {
    case ModuleTypeApplicationBundle:
        return applicationBundleDirectoryExtension;
    case ModuleTypeApplicationExecutable:
        return applicationExecutableFileExtension;
    case ModuleTypeLibraryBundle:
        return libraryBundleDirectoryExtension;
    case ModuleTypeLibraryDynamicBinary:
        return dynamicLibraryBinaryFileExtension;
    case ModuleTypeLibraryStaticBinary:
        return staticLibraryBinaryFileExtension;
    default:
        return unknownExtension;
    }
}

bool ModuleInfo::operator ==(const ModuleInfo &other) const
{
    if (!basicDataEquals(other))
        return false;

    const int bundleLibraryModulesCount = m_bundleLibraryModules.size();
    if (bundleLibraryModulesCount != other.m_bundleLibraryModules.size())
        return false;

    for (int i = 0; i < bundleLibraryModulesCount; ++i) {
        if (!m_bundleLibraryModules[i].basicDataEquals(other))
            return false;
    }

    return true;
}

bool ModuleInfo::operator !=(const ModuleInfo &other) const
{
    return !(*this == other);
}

int ModuleInfo::machoHeaderFileType(const QString &filePath)
{
    int machoHeaderFileType = 0;
    QProcess otool;
    otool.start("otool", QStringList("-h") << filePath);
    otool.waitForFinished();

    const QString otoolResult = otool.readAllStandardOutput();
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    static const QChar lineFeedChar(QChar::LineFeed);
#else
    static const QChar lineFeedChar(QLatin1Char('\n'));
#endif
    const QStringList otoolResultLines = otoolResult.split(lineFeedChar, QString::SkipEmptyParts);
    const int otoolResultLinesCount = otoolResultLines.size();
    static const QString fileTypeColumnHeaderName = "filetype";
    for (int i = 0; i < otoolResultLinesCount; ++i) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
        static const QChar spaceChar(QChar::Space);
#else
        static const QChar spaceChar(' ');
#endif
        const QStringList lineParts = otoolResultLines[i].split(spaceChar, QString::SkipEmptyParts);
        const int fileTypeColumnIndex = lineParts.indexOf(fileTypeColumnHeaderName);
        if (fileTypeColumnIndex >= 0) {
            if (otoolResultLinesCount >= (i + 2)) {
                const QStringList machoHeaderLineParts = otoolResultLines[i + 1].split(spaceChar, QString::SkipEmptyParts);
                if (machoHeaderLineParts.size() == lineParts.size()) {
                    const QString &machoHeaderFileTypeStr = machoHeaderLineParts[fileTypeColumnIndex];
                    machoHeaderFileType = machoHeaderFileTypeStr.toInt();
                    break;
                }
            }
        }
    }

    return machoHeaderFileType;
}

void ModuleInfo::findBundleLibraryFilePaths(const QString &bundlePath)
{
    m_bundleLibraryModules.clear();

    QDirIterator iter(bundlePath,
                       QStringList("*."
                                    + ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)),
                       QDir::Files,
                       QDirIterator::Subdirectories);

    while (iter.hasNext()) {
        iter.next();
        const ModuleInfo moduleInfo(iter.fileInfo().filePath());
        if (moduleInfo.isValid())
            m_bundleLibraryModules << moduleInfo;
    }
}

bool ModuleInfo::verifyInformationPropertyList(
        const QString &infoPlistFilePath,
        const BundleType expectedBundleType,
        const QString &expectedBundleBinaryFileName)
{
    const QFileInfo infoPlistFileInfo(infoPlistFilePath);
    if (!infoPlistFileInfo.exists())
        return false;

    static const QString plistBuddyExePath = "/usr/libexec/PlistBuddy";
    static const QString bundleTypeTagName = "CFBundlePackageType";
    static const QString plistBuddyCommandArgumentsFormat = "%2 %3 -c \"Print %1\"";
    static const QString plistBuddyPrintBundleTypeCommandFormat = plistBuddyCommandArgumentsFormat.arg(bundleTypeTagName);
    QProcess bundlePlistExaminer;
    bundlePlistExaminer.start(plistBuddyPrintBundleTypeCommandFormat.arg(plistBuddyExePath, infoPlistFilePath));
    bundlePlistExaminer.waitForFinished();
    if (bundlePlistExaminer.exitStatus() != QProcess::NormalExit
        || bundlePlistExaminer.exitCode() != EXIT_SUCCESS) {
        return false;
    }

    const QString bundleTypeIdentifier = bundlePlistExaminer.readAllStandardOutput().trimmed();
    if (expectedBundleType == BundleTypeApplication) {
        const QChar separator = QDir::separator();
        static const QString FINDER_APP_DIR_PATH = "/System/Library/CoreServices/Finder." + ModuleInfo::moduleExtension(ModuleTypeApplicationBundle);
        static const QString FINDER_PLIST_FILE_PATH = FINDER_APP_DIR_PATH
                                                      % separator
                                                      % contentsDirName
                                                      % separator
                                                      % informationPropertyListFileName;

        static const QString bundleTypeApplicationFinder = "FNDR";
        static const QString bundleTypeApplicationNormal = "APPL";
        const QString& bundleTypeApplication = infoPlistFileInfo.canonicalFilePath() == FINDER_PLIST_FILE_PATH
                                               ? bundleTypeApplicationFinder
                                               : bundleTypeApplicationNormal;

        if (bundleTypeIdentifier != bundleTypeApplication)
            return false;
    } else if (expectedBundleType == BundleTypeLibrary) {
        static const QString bundleTypeLibrary = "FMWK";
        if (bundleTypeIdentifier != bundleTypeLibrary)
            return false;
    } else {
        return false;
    }

    static const QString bundleBinaryTagName = "CFBundleExecutable";
    static const QString plistBuddyPrintBinaryNameCommandFormat = plistBuddyCommandArgumentsFormat.arg(bundleBinaryTagName);
    bundlePlistExaminer.start(plistBuddyPrintBinaryNameCommandFormat.arg(plistBuddyExePath, infoPlistFilePath));
    bundlePlistExaminer.waitForFinished();
    if (bundlePlistExaminer.exitStatus() != QProcess::NormalExit
         || bundlePlistExaminer.exitCode() != EXIT_SUCCESS) {
        return false;
    }

    const QString bundleBinaryFileName = bundlePlistExaminer.readAllStandardOutput().trimmed();
    if (bundleBinaryFileName != expectedBundleBinaryFileName)
        return false;

    return true;
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream &stream, const ModuleInfo &info)
{
    QString moduleTypeStr;
    switch (info.type()) {
    case ModuleTypeUnknown:
        moduleTypeStr = "unknown";
        break;
    case ModuleTypeApplicationBundle:
        moduleTypeStr = "application bundle";
        break;
    case ModuleTypeApplicationExecutable:
        moduleTypeStr = "application executable";
        break;
    case ModuleTypeLibraryBundle:
        moduleTypeStr = "library bundle";
        break;
    case ModuleTypeLibraryDynamicBinary:
        moduleTypeStr = "dynamic library binary";
        break;
    case ModuleTypeLibraryStaticBinary:
        moduleTypeStr = "static library binary";
        break;
    default:
        break;
    }

    stream << "Module type:" << moduleTypeStr << endl
           << "Binary path:" << info.binaryFilePath() << endl
           << "Bundle path:" << info.path() << endl
           << "Additional libraries:" << endl
           << info.bundleLibraryModules();

    return stream;
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream& stream, const QList<MacDeployQt::Shared::ModuleInfo> &moduleInfos)
{
    foreach (const ModuleInfo &additionalModulesInfo, moduleInfos)
        stream << additionalModulesInfo;

    return stream;
}

bool ModuleInfo::basicDataEquals(const ModuleInfo &other) const
{
    return m_type == other.m_type
           && m_binaryFilePath == other.m_binaryFilePath
           && m_path == other.m_path;
}
