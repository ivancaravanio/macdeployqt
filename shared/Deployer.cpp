/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "deployer.h"

#include "logger.h"
#include "Utilities.h"

#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QLibraryInfo>
#include <QProcess>
#include <QRegExp>
#include <QSet>
#include <QStack>
#include <QString>
#include <QStringBuilder>
#include <QStringList>

#include <iostream>

using namespace MacDeployQt::Shared;

const QString Deployer::s_allCharactersAnyCountWildcard = "*";

bool FrameworkInfo::operator==(const FrameworkInfo &info) const
{
    return ((frameworkPath == info.frameworkPath)
            && (binaryPath == info.binaryPath));
}

bool FrameworkInfo::operator!=(const FrameworkInfo &info) const
{
    return !(*this == info);
}

QString FrameworkInfo::description() const
{
    QString desc;
    QTextStream ts( & desc, QIODevice::WriteOnly );
    ts << "Framework name:        " << this->frameworkName << "\r\n"
       << "Framework directory:   " << this->frameworkDirectory << "\r\n"
       << "Framework path:        " << this->frameworkPath << "\r\n"
       << "Binary directory:      " << this->binaryDirectory << "\r\n"
       << "Binary name:           " << this->binaryName << "\r\n"
       << "Binary path:           " << this->binaryPath << "\r\n"
       << "Version:               " << this->version << "\r\n"
       << "Install name:          " << this->installName << "\r\n"
       << "Deployed install name: " << this->deployedInstallName << "\r\n"
       << "Source file Path:      " << this->sourceFilePath << "\r\n"
       << "Framework Destination Directory (relative to bundle): " << this->frameworkDestinationDirectory << "\r\n"
       << "Binary Destination Directory (relative to bundle):    " << this->binaryDestinationDirectory;

    return desc;
}

void Deployer::changeQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QString &qtPath,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<ModuleInfo> modules;
    modules << mainModuleInfo
            << mainModuleInfo.bundleLibraryModules();
    const QList<FrameworkInfo> frameworks = getQtFrameworksForPaths(mainModuleInfo,
                                                                    modules,
                                                                    getBinaryRPaths(mainModuleInfo.binaryFilePath()),
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
    if (frameworks.isEmpty()) {
        Logger::instance().logWarning() << "";
        Logger::instance().logWarning() << "Could not find any _external_ Qt frameworks to change in" << mainModuleInfo;
        return;
    }

    const QString absoluteQtPath = QDir(qtPath).absolutePath();
    changeQtFrameworks(frameworks, modules, absoluteQtPath);
}

void Deployer::deployQtFrameworks(
        const ModuleInfo&        aMainModuleInfo,
        const QList<ModuleInfo>& aAdditionalExecutables,
        const bool               aShouldUseDebugLibs,
        const bool               aShouldDeployPlugins,
        const bool               aShouldCreateDmg,
        const bool               aShouldStrip,
        const QString&           aQmlDir,
        const bool               aAlwaysOverwrite,
        const bool               aShouldCodeSign,
        const QString&           aCodeSignIdentity,
        const bool               aIsAppStoreCompliant,
        const QStringList&       aAdditionalLibraryPaths )
{
    DeploymentInfo deploymentInfo = deployQtFrameworks(aMainModuleInfo,
                                                       aAdditionalExecutables,
                                                       aShouldUseDebugLibs,
                                                       aShouldStrip,
                                                       aAlwaysOverwrite,
                                                       aAdditionalLibraryPaths);
    if (aShouldDeployPlugins && !deploymentInfo.qtPath.isEmpty()) {
        deploymentInfo.pluginPath = deploymentInfo.qtPath % QDir::separator() % "plugins";

        deployPlugins(aMainModuleInfo,
                      deploymentInfo,
                      aShouldUseDebugLibs,
                      aShouldStrip,
                      aAlwaysOverwrite,
                      aIsAppStoreCompliant,
                      aAdditionalLibraryPaths);
        createQtConf(aMainModuleInfo);
    }

    QStringList qmlDirs;
    // Convenience: Look for .qml files in the current directoty if no -qmldir specified.
    if (aQmlDir.isEmpty()) {
        QDir dir;
        if (!dir.entryList(QStringList() << QStringLiteral("*.qml")).isEmpty()) {
            qmlDirs.append(QStringLiteral("."));
        }
    } else {
        qmlDirs.append(aQmlDir);
    }

    if (!qmlDirs.isEmpty())
    {
        deployQmlImports(aMainModuleInfo,
                         deploymentInfo,
                         qmlDirs,
                         aShouldUseDebugLibs,
                         aShouldStrip,
                         aAlwaysOverwrite,
                         aAdditionalLibraryPaths);
    }

    if (aShouldStrip)
        stripModuleBinary(aMainModuleInfo);

    if (aShouldCodeSign)
        codesign(aMainModuleInfo, aCodeSignIdentity);

    if (aShouldCreateDmg)
        createDiskImage(aMainModuleInfo, aAlwaysOverwrite);
}

Deployer::DeploymentInfo::DeploymentInfo()
    : useLoaderPath(false)
{
}

bool Deployer::copyFilePrintStatus(
        const QString &from,
        const QString &to,
        const bool alwaysOverwrite)
{
    if (QFile(to).exists()) {
        if (alwaysOverwrite) {
            QFile(to).remove();
        } else {
            Logger::instance().logDebug() << "File exists, skip copy:" << to;
            return false;
        }
    }

    if (!QFile::copy(from, to)) {
        Logger::instance().logError() << "file copy failed from" << from;
        Logger::instance().logError() << " to" << to;
        return false;
    }

    QFile dest(to);
    dest.setPermissions(dest.permissions() | QFile::WriteOwner | QFile::WriteUser);
    Logger::instance().logInfo() << " copied:" << from;
    Logger::instance().logInfo() << " to" << to;

    // The source file might not have write permissions set. Set the
    // write permission on the target file to make sure we can use
    // install_name_tool on it later.
    QFile toFile(to);
    if (toFile.permissions() & QFile::WriteOwner)
        return true;

    if (!toFile.setPermissions(toFile.permissions() | QFile::WriteOwner)) {
        Logger::instance().logError() << "Failed to set u+w permissions on target file: " << to;
        return false;
    }

    return true;
}

bool Deployer::linkFilePrintStatus(const QString &file, const QString &link)
{
    if (QFile(link).exists()) {
        if (QFile(link).symLinkTarget().isEmpty())
            Logger::instance().logError() << link << "exists but it's a file.";
        else
            Logger::instance().logInfo() << "Symlink exists, skipping:" << link;
        return false;
    } else if (QFile::link(file, link)) {
        Logger::instance().logInfo() << " symlink" << link;
        Logger::instance().logInfo() << " points to" << file;
        return true;
    } else {
        Logger::instance().logError() << "failed to symlink" << link;
        Logger::instance().logError() << " to" << file;
        return false;
    }
}

void Deployer::patch_debugInInfoPlist(const QString &infoPlistPath)
{
    // Older versions of qmake may have the "_debug" binary as
    // the value for CFBundleExecutable. Remove it.
    QFile infoPlist(infoPlistPath);
    infoPlist.open(QIODevice::ReadOnly);
    QByteArray contents = infoPlist.readAll();
    infoPlist.close();
    infoPlist.open(QIODevice::WriteOnly | QIODevice::Truncate);
    contents.replace("_debug", ""); // surely there are no legit uses of "_debug" in an Info.plist
    infoPlist.write(contents);
}

FrameworkInfo Deployer::parseOtoolLibraryLine(
        const ModuleInfo &mainModuleInfo,
        const QString &line,
        const QSet<QString> &rpaths,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    FrameworkInfo info;
    QString trimmed = line.trimmed();

    if (trimmed.isEmpty())
        return info;

    // Don't deploy system libraries.
    if (trimmed.startsWith("/System/Library/")
        || (trimmed.startsWith("/usr/lib/") && trimmed.contains("libQt") == false) // exception for libQtuitools and libQtlucene
        || trimmed.startsWith(ModuleInfo::exePath)
        || trimmed.startsWith(ModuleInfo::loaderPath)) {
        return info;
    }

    const QChar dirSeparator = QDir::separator();

    const QString mainModulePath = mainModuleInfo.path();
    // Resolve rpath relative libraries.
    const QString rpathSlashed = ModuleInfo::rPath + dirSeparator;
    if (trimmed.startsWith(rpathSlashed)) {
        trimmed = trimmed.mid(rpathSlashed.length());
        bool foundInsideBundle = false;
        for (const QString &rpath : rpaths) {
            QString path = QDir::cleanPath(rpath % dirSeparator % trimmed);
            // Skip paths already inside the bundle.
            if (!mainModulePath.isEmpty()) {
                if (QDir::isAbsolutePath(mainModulePath)) {
                    if (path.startsWith(QDir::cleanPath(mainModulePath) + dirSeparator)) {
                        foundInsideBundle = true;
                        continue;
                    }
                } else {
                    if (path.startsWith(QDir::cleanPath(QDir::currentPath() % dirSeparator % mainModulePath) + dirSeparator)) {
                        foundInsideBundle = true;
                        continue;
                    }
                }
            }
            // Try again with substituted rpath.
            FrameworkInfo resolvedInfo = Deployer::parseOtoolLibraryLine(mainModuleInfo, path, rpaths, useDebugLibs, additionalLibraryPaths);
            if (!resolvedInfo.frameworkName.isEmpty() && QFile::exists(resolvedInfo.frameworkPath)) {
                resolvedInfo.rpathUsed = rpath;
                return resolvedInfo;
            }
        }
        if (!rpaths.isEmpty() && !foundInsideBundle) {
            Logger::instance().logError() << "Cannot resolve rpath" << trimmed;
            Logger::instance().logError() << " using" << rpaths.toList().join( ", " );
        }
        return info;
    }

    enum State {QtPath, FrameworkName, DylibName, Version, End};
    State state = QtPath;
    int part = 0;
    QString name;
    QString qtPath;

    QString suffix;
    if (useDebugLibs) {
        static const QString debugSuffix = "_debug";
        suffix = debugSuffix;
    }

    // Split the line into [Qt-path]/lib/qt[Module].framework/Versions/[Version]/
    QStringList parts = trimmed.split(dirSeparator);
    while (part < parts.count()) {
        const QString currentPart = parts.at(part).simplified() ;
        ++part;
        if (currentPart.isEmpty())
            continue;

        static const QString dynamicLibraryBinaryExtension = QChar(QLatin1Char('.'))
                                                             % ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)
                                                             % QChar(QLatin1Char(' '));
        static const QString libraryBundleFolderExtension = QChar(QLatin1Char('.'))
                                                            + ModuleInfo::moduleExtension(ModuleTypeLibraryBundle);
        if (state == QtPath) {
            // Check for library name part
            if (part < parts.count() && parts.at(part).contains(dynamicLibraryBinaryExtension)) {
                info.installName += dirSeparator + static_cast< QString >(qtPath % currentPart % dirSeparator).simplified();
                info.frameworkDirectory = info.installName;
                state = DylibName;
                continue;
            } else if (part < parts.count() && parts.at(part).endsWith(libraryBundleFolderExtension)) {
                info.installName += dirSeparator + static_cast< QString >(qtPath % "lib" % dirSeparator).simplified();
                info.frameworkDirectory = info.installName;
                state = FrameworkName;
                continue;
            } else if (! trimmed.startsWith(dirSeparator)) {      // If the line does not contain a full path, the app is using a binary Qt package.
                if (currentPart.contains(libraryBundleFolderExtension)) {
                    info.frameworkDirectory = "/Library/Frameworks/";
                    state = FrameworkName;
                } else {
                    info.frameworkDirectory = "/usr/lib/";
                    state = DylibName;
                }

                --part;
                continue;
            }

            qtPath += (currentPart + dirSeparator);
        }

        if (state == FrameworkName) {
            // remove ".framework"
            name = currentPart;
            name.chop(libraryBundleFolderExtension.length());
            info.isDylib = false;
            info.frameworkName = currentPart;
            state = Version;
            ++part;
            continue;
        }

        if (state == DylibName) {
            name = currentPart.split(" (compatibility").first();
            info.isDylib = true;
            info.frameworkName = name;
            info.binaryName = name.contains("Qt") && (suffix.isEmpty() ? true : !name.contains(suffix))
                              ? name.left(name.indexOf('.')) % suffix % name.mid(name.indexOf('.'))
                              : name;

            // search the additional directories for dependent libraries' binaries
            if (!QFileInfo(QDir(info.frameworkDirectory), info.binaryName).exists()) {
                for (const QString &additionalLibraryPath : additionalLibraryPaths) {
                    if (QFileInfo(QDir(additionalLibraryPath), info.binaryName).exists()) {
                        info.frameworkDirectory = additionalLibraryPath + dirSeparator;
                        break;
                    }
                }
            }

            info.installName += name;
            info.deployedInstallName = mainModuleInfo.librariesInstallNameDirPath()
                                       % dirSeparator
                                       % info.binaryName;
            info.frameworkPath = info.frameworkDirectory + info.binaryName;
            info.sourceFilePath = info.frameworkPath;
            info.frameworkDestinationDirectory = mainModuleInfo.librariesRelativeDirPath()
                                                 + dirSeparator;
            info.binaryDestinationDirectory = info.frameworkDestinationDirectory;
            info.binaryDirectory = info.frameworkDirectory;
            info.binaryPath = info.frameworkPath;
            state = End;
            ++part;
            continue;
        }
        else if (state == Version) {
            info.version = currentPart;
            info.binaryDirectory = "Versions/" + info.version;
            info.binaryName = name + suffix;
            info.binaryPath = dirSeparator % info.binaryDirectory % dirSeparator % info.binaryName;
            info.installName += info.frameworkName % dirSeparator % info.binaryDirectory % dirSeparator % name;
            info.deployedInstallName = mainModuleInfo.librariesInstallNameDirPath()
                                       % dirSeparator
                                       % info.frameworkName
                                       % info.binaryPath;
            // search the additional directories for dependent frameworks' bundles
            if (!QDir(info.frameworkDirectory + info.frameworkName).exists()) {
                for (const QString &additionalLibraryPath : additionalLibraryPaths) {
                    if (QDir(additionalLibraryPath % dirSeparator % info.frameworkName).exists()) {
                        info.frameworkDirectory = additionalLibraryPath + dirSeparator;
                        break;
                    }
                }
            }
            info.frameworkPath = info.frameworkDirectory + info.frameworkName;
            info.sourceFilePath = info.frameworkPath + info.binaryPath;
            info.frameworkDestinationDirectory = mainModuleInfo.librariesRelativeDirPath()
                                                 % dirSeparator
                                                 % info.frameworkName;
            info.binaryDestinationDirectory = info.frameworkDestinationDirectory
                                              % dirSeparator
                                              % info.binaryDirectory;
            state = End;
        } else if (state == End) {
            break;
        }
    }

    return info;
}

QSet<QString> Deployer::getBinaryRPaths(const QString &path, const bool resolve, const QString &executablePath)
{
    QSet<QString> rpaths;

    QStringList outputLines;
    QString errorMessage;
    if ( ! Deployer::binaryLoadCommands( path, & outputLines, & errorMessage ) )
    {
        Logger::instance().logError() << errorMessage;
    }

    const QString &actualExecutablePath = resolve && executablePath.isEmpty()
                                          ? path
                                          : executablePath;
    QStringListIterator i(outputLines);

    while (i.hasNext()) {
        if (i.next().contains("cmd LC_RPATH") && i.hasNext()
            && i.next().contains("cmdsize") && i.hasNext()) {
            const QString &rpathCmd = i.next();
            int pathStart = rpathCmd.indexOf("path ");
            int pathEnd = rpathCmd.indexOf(" (");
            if (pathStart >= 0 && pathEnd >= 0 && pathStart < pathEnd) {
                QString rpath = rpathCmd.mid(pathStart + 5, pathEnd - pathStart - 5);
                if (resolve) {
                    rpaths << resolveDyldPrefix(rpath, path, actualExecutablePath);
                } else {
                    rpaths << rpath;
                }
            }
        }
    }

    return rpaths;
}

QString Deployer::resolveDyldPrefix(const QString& path, const QString& loaderPath, const QString& executablePath)
{
    if (! path.startsWith(QLatin1Char('@'))) {
        return path;
    }

    const QChar dirSeparator = QDir::separator();
    bool isExe = false;
    if (path.startsWith(ModuleInfo::exePath + dirSeparator)) {
        // path relative to bundle executable dir
        isExe = true;
    } else if (path.startsWith(ModuleInfo::loaderPath + dirSeparator)) {
        // path relative to loader dir
        isExe = false;
    } else {
        Logger::instance().logError() << "Unexpected prefix" << path;
        return path;
    }

    const QString& basePath = isExe
                              ? executablePath
                              : loaderPath;
    return QDir::cleanPath((QDir::isAbsolutePath(isExe
                                                 ? executablePath
                                                 : loaderPath)
                            ? QString()
                            : ( QDir::currentPath() + dirSeparator))
                           % QFileInfo(basePath).path()
                           % path.mid((isExe
                                       ? ModuleInfo::exePath
                                       : ModuleInfo::loaderPath).length()));
}

void Deployer::deployRPaths(const ModuleInfo &mainModuleInfo, const QSet<QString> &rpaths, const ModuleInfo &dependencyModule, bool useLoaderPath)
{
    const QString absFrameworksPath = mainModuleInfo.librariesAbsoluteDirPath();
    const QString dependencyModuleBinaryFilePath = dependencyModule.binaryFilePath();
    const QString relativeFrameworkPath = QFileInfo(dependencyModuleBinaryFilePath).absoluteDir().relativeFilePath(absFrameworksPath);

    const QChar dirSeparator = QDir::separator();
    const QString loaderPathToFrameworks = ModuleInfo::loaderPath % dirSeparator % relativeFrameworkPath;
    bool rpathToFrameworksFound = false;
    QStringList args;
    static const QString frameworksExeRpath = ModuleInfo::exePath % dirSeparator
                                              % QLatin1String( ".." ) % dirSeparator
                                              % ModuleInfo::frameworksDirName;
    for (const QString &rpath : getBinaryRPaths(dependencyModuleBinaryFilePath, false)) {
        if (rpath == frameworksExeRpath
            || rpath == loaderPathToFrameworks) {
            rpathToFrameworksFound = true;
            continue;
        }
        if (rpaths.contains(resolveDyldPrefix(rpath, dependencyModuleBinaryFilePath, dependencyModuleBinaryFilePath))) {
            args << "-delete_rpath" << rpath;
        }
    }
    if (!args.length()) {
        return;
    }
    if (!rpathToFrameworksFound) {
        args << "-add_rpath" << (useLoaderPath
                                 ? loaderPathToFrameworks
                                 : frameworksExeRpath);
    }
    Logger::instance().logDebug() << "Using install_name_tool:";
    Logger::instance().logDebug() << " change rpaths in" << dependencyModuleBinaryFilePath;
    Logger::instance().logDebug() << " using" << args.join( ", " );
    runInstallNameTool(args << dependencyModuleBinaryFilePath);
}

void Deployer::deployRPaths(const ModuleInfo &mainModuleInfo, const QSet<QString> &rpaths, const QList<ModuleInfo> &dependencyModules, bool useLoaderPath)
{
    for (const ModuleInfo &dependencyModule : dependencyModules) {
        deployRPaths(mainModuleInfo, rpaths, dependencyModule, useLoaderPath);
    }
}


QList<FrameworkInfo> Deployer::getQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QStringList &otoolLines,
        const QSet<QString> &rpaths,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<FrameworkInfo> libraries;
    for (const QString &line : otoolLines) {
        const FrameworkInfo info = parseOtoolLibraryLine(mainModuleInfo, line, rpaths, useDebugLibs, additionalLibraryPaths);
        if (!info.frameworkName.isEmpty()) {
            Logger::instance().logDebug() << "Adding framework:";
            Logger::instance().logDebug() << info;
            libraries.append(info);
        }
    }
    return libraries;
}

QList<FrameworkInfo> Deployer::getQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const ModuleInfo &moduleInfo,
        const QSet<QString> &rpaths,
        bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    if (!moduleInfo.isValid())
        return QList<FrameworkInfo>();

    const QString binaryFilePath = moduleInfo.binaryFilePath();

    Logger::instance().logDebug() << "Using otool:";
    Logger::instance().logDebug() << " inspecting" << binaryFilePath;

    QStringList outputLines;
    QString errorMessage;
    if ( ! Deployer::binarySharedLibraries( binaryFilePath, & outputLines, & errorMessage ) )
    {
        Logger::instance().logError() << errorMessage;
    }

    outputLines.removeFirst(); // remove line containing the binary path
    if (moduleInfo.isLibrary())
        outputLines.removeFirst(); // frameworks and dylibs print install name of themselves first.

    return getQtFrameworks(mainModuleInfo,
                           outputLines,
                           rpaths + getBinaryRPaths(binaryFilePath),
                           useDebugLibs,
                           additionalLibraryPaths);
}

QList<FrameworkInfo> Deployer::getQtFrameworksForPaths(
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &modules,
        const QSet<QString> &rpaths,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<FrameworkInfo> result;
    QSet<QString> existing;
    for (const ModuleInfo &module : modules) {
        const QList<FrameworkInfo> moduleDependencies = getQtFrameworks(mainModuleInfo, module, rpaths, useDebugLibs, additionalLibraryPaths);
        for (const FrameworkInfo &dependency : moduleDependencies ) {
            if (!existing.contains(dependency.frameworkPath)) { // avoid duplicates
                existing.insert(dependency.frameworkPath);
                result << dependency;
            }
        }
    }
    return result;
}

// copies everything _inside_ sourcePath to destinationPath
bool Deployer::recursiveCopy(
        const QString &sourcePath,
        const QString &destinationPath,
        const bool alwaysOverwrite)
{
    if (!QDir(sourcePath).exists())
        return false;

    static const QStringList allEntities(s_allCharactersAnyCountWildcard);
    const QStringList files = QDir(sourcePath).entryList(allEntities, QDir::Files | QDir::NoDotAndDotDot);
    const QStringList subdirs = QDir(sourcePath).entryList(allEntities, QDir::Dirs | QDir::NoDotAndDotDot);
    if (files.empty() && subdirs.empty())
        return true;

    QDir().mkpath(destinationPath);

    const QChar dirSeparator = QDir::separator();
    for (const QString &file : files) {
        const QString fileSourcePath = sourcePath % dirSeparator % file;
        const QString fileDestinationPath = destinationPath % dirSeparator % file;
        copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite);
    }

    for (const QString &dir : subdirs) {
        recursiveCopy(sourcePath % dirSeparator % dir,
                      destinationPath % dirSeparator % dir,
                      alwaysOverwrite);
    }

    return true;
}

void Deployer::recursiveCopyAndDeploy(
        const ModuleInfo &mainModuleInfo,
        const QSet<QString> &rpaths,
        const QString &sourcePath,
        const QString &destinationPath,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    QDir().mkpath(destinationPath);

    Logger::instance().logInfo() << "copy:" << sourcePath << destinationPath;

    const QChar dirSeparator = QDir::separator();
    const QStringList files = QDir(sourcePath).entryList(QStringList() << s_allCharactersAnyCountWildcard, QDir::Files | QDir::NoDotAndDotDot);
    foreach (const QString &file, files) {
        const QString fileSourcePath = sourcePath % dirSeparator % file;
        const QString fileDestinationPath = destinationPath % dirSeparator % file;

        if (file.endsWith("_debug.dylib")) {
            continue; // Skip debug versions
        } else if (file.endsWith(QStringLiteral(".dylib"))) {
            if (copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite)) {
                const QStringList destinationFilePaths(fileDestinationPath);
                QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo, destinationFilePaths, rpaths, useDebugLibs, additionalLibraryPaths);

                const int destinationFilesCount = destinationFilePaths.size();
                QList< ModuleInfo > destionationModules;
                destionationModules.reserve(destinationFilesCount);
                for (int i = 0; i < destinationFilesCount; ++ i) {
                    destionationModules.append(ModuleInfo(destinationFilePaths[i]));
                }

                deployQtFrameworks(frameworks,
                                   mainModuleInfo,
                                   destionationModules,
                                   useDebugLibs,
                                   useLoaderPath,
                                   runStrip,
                                   alwaysOverwrite,
                                   additionalLibraryPaths);
            }
        } else {
            copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite);
        }
    }

    const QStringList subdirs = QDir(sourcePath).entryList(QStringList() << s_allCharactersAnyCountWildcard, QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString &dir, subdirs) {
        recursiveCopyAndDeploy(mainModuleInfo,
                               rpaths,
                               sourcePath % dirSeparator % dir,
                               destinationPath % dirSeparator % dir,
                               useDebugLibs,
                               useLoaderPath,
                               runStrip,
                               alwaysOverwrite,
                               additionalLibraryPaths);
    }
}

QString Deployer::copyDylib(
        const FrameworkInfo &framework,
        const QString& path,
        const bool alwaysOverwrite)
{
    Logger& logger = Logger::instance();
    if (!QFile::exists(framework.sourceFilePath)) {
        logger.logError() << "no file at" << framework.sourceFilePath;
        return QString();
    }

    const QChar dirSeparator = QDir::separator();

    // Construct destination paths. The full path typically looks like
    // MyApp.app/Contents/Frameworks/libfoo.dylib
    const QString dylibDestinationDirectory = path % dirSeparator % framework.frameworkDestinationDirectory;
    const QString dylibDestinationBinaryPath = dylibDestinationDirectory % dirSeparator % framework.binaryName;

    // Create destination directory
    if (!QDir().mkpath(dylibDestinationDirectory)) {
        logger.logError() << "could not create destination directory" << dylibDestinationDirectory;
        return QString();
    }

    // Retrun if the dylib has aleardy been deployed
    if (QFileInfo(dylibDestinationBinaryPath).exists() && !alwaysOverwrite)
        return dylibDestinationBinaryPath;

    // Copy dylib binary
    copyFilePrintStatus(framework.sourceFilePath, dylibDestinationBinaryPath, alwaysOverwrite);
    return dylibDestinationBinaryPath;
}

QString Deployer::copyFramework(
        const FrameworkInfo &framework,
        const ModuleInfo &mainModuleInfo,
        const bool alwaysOverwrite)
{
    if (!QFile::exists(framework.sourceFilePath)) {
        Logger::instance().logError() << "no file at" << framework.sourceFilePath;
        return QString();
    }

    const QChar dirSeparator = QDir::separator();

    // Construct destination paths. The full path typically looks like
    // MyApp.app/Contents/Frameworks/Foo.framework/Versions/5/QtFoo
    const QString frameworkDestinationDirectory = mainModuleInfo.path() % dirSeparator % framework.frameworkDestinationDirectory;
    const QString frameworkBinaryDestinationDirectory = frameworkDestinationDirectory % dirSeparator % framework.binaryDirectory;
    const QString frameworkDestinationBinaryPath = frameworkBinaryDestinationDirectory % dirSeparator % framework.binaryName;

    // Return if the framework has aleardy been deployed
    if (QDir(frameworkDestinationDirectory).exists() && !alwaysOverwrite)
        return QString();

    // Create destination directory
    if (!QDir().mkpath(frameworkBinaryDestinationDirectory)) {
        Logger::instance().logError() << "could not create destination directory" << frameworkBinaryDestinationDirectory;
        return QString();
    }

    // Now copy the framework. Some parts should be left out (headers/, .prl files).
    // Some parts should be included (Resources/, symlink structure). We want this
    // function to make as few assumtions about the framework as possible while at
    // the same time producing a codesign-compatible framework.

    // Copy framework binary
    copyFilePrintStatus(framework.sourceFilePath, frameworkDestinationBinaryPath, alwaysOverwrite);

    // Copy Resouces/, Libraries/ and Helpers/

    const QString resourcesSourcePath = framework.frameworkPath % dirSeparator % ModuleInfo::resourcesDirName;
    const QString resourcesDestianationPath = frameworkDestinationDirectory % dirSeparator
                                              % ModuleInfo::versionsDirName % dirSeparator
                                              % framework.version % dirSeparator
                                              % ModuleInfo::resourcesDirName;
    recursiveCopy(resourcesSourcePath, resourcesDestianationPath, alwaysOverwrite);
    const QString librariesSourcePath = framework.frameworkPath % dirSeparator % ModuleInfo::librariesDirName;
    const QString librariesDestianationPath = frameworkDestinationDirectory % dirSeparator
                                              % ModuleInfo::versionsDirName % dirSeparator
                                              % framework.version % dirSeparator
                                              % ModuleInfo::librariesDirName;;
    bool createdLibraries = recursiveCopy(librariesSourcePath, librariesDestianationPath, alwaysOverwrite);
    const QString helpersSourcePath = framework.frameworkPath % dirSeparator % ModuleInfo::helpersDirName;
    const QString helpersDestianationPath = frameworkDestinationDirectory % dirSeparator
                                            % ModuleInfo::versionsDirName % dirSeparator
                                            % ModuleInfo::helpersDirName;
    bool createdHelpers = recursiveCopy(helpersSourcePath, helpersDestianationPath, alwaysOverwrite);

    // Create symlink structure. Links at the framework root point to Versions/Current/
    // which again points to the actual version:
    // QtFoo.framework/QtFoo -> Versions/Current/QtFoo
    // QtFoo.framework/Resources -> Versions/Current/Resources
    // QtFoo.framework/Versions/Current -> 5
    linkFilePrintStatus(ModuleInfo::versionsDirName % dirSeparator
                        % ModuleInfo::currentVersionDirName % dirSeparator
                        % framework.binaryName,
                        frameworkDestinationDirectory % dirSeparator
                        % framework.binaryName);
    linkFilePrintStatus(ModuleInfo::versionsDirName % dirSeparator
                        % ModuleInfo::currentVersionDirName % dirSeparator
                        % ModuleInfo::resourcesDirName,
                        frameworkDestinationDirectory % dirSeparator
                        % ModuleInfo::resourcesDirName);
    if (createdLibraries)
        linkFilePrintStatus(ModuleInfo::versionsDirName % dirSeparator
                            % ModuleInfo::currentVersionDirName % dirSeparator
                            % ModuleInfo::librariesDirName,
                            frameworkDestinationDirectory  % dirSeparator
                            % ModuleInfo::librariesDirName);
    if (createdHelpers)
        linkFilePrintStatus(ModuleInfo::versionsDirName % dirSeparator
                            % ModuleInfo::currentVersionDirName % dirSeparator
                            % ModuleInfo::helpersDirName,
                            frameworkDestinationDirectory % dirSeparator
                            % ModuleInfo::helpersDirName);
    linkFilePrintStatus(framework.version, frameworkDestinationDirectory + "/Versions/Current");

    // Correct Info.plist location for frameworks produced by older versions of qmake
    // Contents/Info.plist should be Versions/5/Resources/Info.plist
    const QString legacyInfoPlistPath = framework.frameworkPath % dirSeparator % ModuleInfo::contentsDirName % dirSeparator % ModuleInfo::informationPropertyListFileName;
    const QString correctInfoPlistPath = frameworkDestinationDirectory % dirSeparator % ModuleInfo::resourcesDirName % dirSeparator % ModuleInfo::informationPropertyListFileName;
    if (QFile(legacyInfoPlistPath).exists()) {
        copyFilePrintStatus(legacyInfoPlistPath, correctInfoPlistPath, alwaysOverwrite);
        patch_debugInInfoPlist(correctInfoPlistPath);
    }

    return frameworkDestinationBinaryPath;
}

void Deployer::runInstallNameTool(const QStringList &options)
{
    QString output;
    QString errorMessage;
    if ( ! Deployer::syncExecuteTool( "install_name_tool", options, & output, & errorMessage ) )
    {
        Logger::instance().logError() << errorMessage;
    }

    Logger::instance().logInfo() << output;
}

void Deployer::changeIdentification(const QString &id, const QString &binaryPath)
{
    Logger::instance().logDebug() << "Using install_name_tool:";
    Logger::instance().logDebug() << " change identification in" << binaryPath;
    Logger::instance().logDebug() << " to" << id;
    runInstallNameTool(QStringList() << "-id" << id << binaryPath);
}

void Deployer::changeInstallName(
        const ModuleInfo &moduleInfo,
        const FrameworkInfo &framework,
        const QList<ModuleInfo> &modules,
        const bool useLoaderPath)
{
    foreach (const ModuleInfo &module, modules) {
        QString deployedInstallName;
        if (useLoaderPath) {
            const QChar dirSep = QDir::separator();
            const QString dependencyBinaryPath = moduleInfo.workingDirPath()
                                                 % dirSep
                                                 % framework.binaryDestinationDirectory
                                                 % dirSep
                                                 % framework.binaryName;

            deployedInstallName =
                    ModuleInfo::loaderPath
                    % dirSep
                    % QFileInfo(module.binaryFilePath())
                      .absoluteDir()
                      .relativeFilePath(dependencyBinaryPath);
        } else {
            deployedInstallName = framework.deployedInstallName;
        }
        changeInstallName(framework.installName, deployedInstallName, module);
    }
}

void Deployer::changeInstallName(const QString &oldName, const QString &newName, const ModuleInfo &moduleInfo)
{
    Logger::instance().logDebug() << "Using install_name_tool:";
    Logger::instance().logDebug() << " in" << moduleInfo;
    Logger::instance().logDebug() << " change reference" << oldName;
    Logger::instance().logDebug() << " to" << newName;
    runInstallNameTool(QStringList() << "-change" << oldName << newName << moduleInfo.binaryFilePath());
}

void Deployer::runStrip(const QString &binaryPath)
{
    Logger::instance().logDebug() << "Using strip:";
    Logger::instance().logDebug() << " stripped" << binaryPath;
    QProcess strip;
    strip.start("strip", QStringList() << "-x" << binaryPath);
    strip.waitForFinished();
    if (strip.exitStatus() != QProcess::NormalExit
        || strip.exitCode() != EXIT_SUCCESS) {
        Logger::instance().logError() << strip.readAllStandardError();
        Logger::instance().logError() << strip.readAllStandardOutput();
    }
}

/*
    Deploys the the listed frameworks listed into an app bundle.
    The frameworks are searched for dependencies, which are also deployed.
    (deploying Qt3Support will also deploy QtNetwork and QtSql for example.)
    Returns a DeploymentInfo structure containing the Qt path used and a
    a list of actually deployed frameworks.
*/
Deployer::DeploymentInfo Deployer::deployQtFrameworks(
        const QList<FrameworkInfo> &frameworks,
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &modules,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying Qt frameworks found inside:" << modules;
    QStringList copiedFrameworks;
    DeploymentInfo deploymentInfo;
    deploymentInfo.useLoaderPath = useLoaderPath;
    QSet<QString> rpathsUsed;

    QList<FrameworkInfo> frameworksInfo = frameworks;
    while (!frameworksInfo.isEmpty()) {
        const FrameworkInfo framework = frameworksInfo.takeFirst();
        copiedFrameworks.append(framework.frameworkName);

        /* Get the qt path from the QtCore module, since all other Qt modules depend on it
         * Do not check using framework.frameworkName.contains("Qt"),
         * since Qt addon modules also start with Qt and they could be located outside the
         * Qt library's directory used by the module being deployed.
         */
        if (deploymentInfo.qtPath.isNull()
            && framework.frameworkName.startsWith("QtCore")
            && framework.frameworkDirectory.contains("/lib")) {
            deploymentInfo.qtPath = framework.frameworkDirectory;
            deploymentInfo.qtPath.chop(5); // remove "/lib/"
        }

        const QChar dirSeparator = QDir::separator();
        static const QString separatedExePath = ModuleInfo::exePath
                                                % dirSeparator;
        static const QString separatedRPath = ModuleInfo::rPath
                                              % dirSeparator;
        if (framework.installName.startsWith(separatedExePath)
            || framework.installName.startsWith(separatedRPath)) {
            Logger::instance().logError() << framework.frameworkName << "already deployed, skipping.";
            continue;
        }

        // Install_name_tool the new id into the binaries
        if (framework.rpathUsed.isEmpty()) {
            changeInstallName(mainModuleInfo, framework, modules, useLoaderPath);
        } else {
            rpathsUsed << framework.rpathUsed;
        }

        // Copy the framework/dylib to the app bundle.
        const QString deployedBinaryPath = framework.isDylib
                                           ? copyDylib(framework, mainModuleInfo.path(), alwaysOverwrite)
                                           : copyFramework(framework, mainModuleInfo, alwaysOverwrite);
        // Skip the rest if already was deployed.
        if (deployedBinaryPath.isNull())
            continue;

        if (runStrip)
            Deployer::runStrip(deployedBinaryPath);

        // Install_name_tool it a new id.
        if (!framework.rpathUsed.length()) {
            changeIdentification(framework.deployedInstallName, deployedBinaryPath);
        }

        // Install_name_tool it a new id.
        changeIdentification(framework.deployedInstallName, deployedBinaryPath);
        // Check for framework dependencies

        const ModuleInfo deployedFramework(deployedBinaryPath);
        const QList<FrameworkInfo> dependencies = getQtFrameworks(mainModuleInfo, deployedFramework, rpathsUsed, useDebugLibs, additionalLibraryPaths);

        QList<ModuleInfo> deployedFrameworks;
        deployedFrameworks << deployedFramework;
        for (const FrameworkInfo &dependency : dependencies) {
            if (dependency.rpathUsed.isEmpty()) {
                changeInstallName(mainModuleInfo, dependency, deployedFrameworks, useLoaderPath);
            } else {
                rpathsUsed << dependency.rpathUsed;
            }

            // Deploy framework if necessary.
            if (!copiedFrameworks.contains(dependency.frameworkName)
                && !frameworksInfo.contains(dependency)) {
                frameworksInfo.append(dependency);
            }
        }
    }
    deploymentInfo.deployedFrameworks = copiedFrameworks;
    deployRPaths(mainModuleInfo, rpathsUsed, modules, useLoaderPath);
    deploymentInfo.rpathsUsed += rpathsUsed;
    return deploymentInfo;
}

Deployer::DeploymentInfo Deployer::deployQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &additionalExecutables,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    QList<ModuleInfo> modules;
    modules << mainModuleInfo
            << mainModuleInfo.bundleLibraryModules()
            << additionalExecutables;

    const QList<FrameworkInfo> frameworks = getQtFrameworksForPaths(mainModuleInfo,
                                                                    modules,
                                                                    getBinaryRPaths(mainModuleInfo.binaryFilePath()),
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
    if (frameworks.isEmpty() && !alwaysOverwrite) {
        Logger::instance().logWarning() << "Could not find any external Qt frameworks to deploy for" << mainModuleInfo.path();
        Logger::instance().logWarning() << "Perhaps macdeployqt was already used on" << mainModuleInfo.path() << "?";
        Logger::instance().logWarning() << "If so, you will need to rebuild" << mainModuleInfo.path() << "before trying again.";
        return DeploymentInfo();
    }

    return deployQtFrameworks(frameworks,
                              mainModuleInfo,
                              modules,
                              useDebugLibs,
                              !additionalExecutables.isEmpty() || mainModuleInfo.isLibrary(),
                              runStrip,
                              alwaysOverwrite,
                              additionalLibraryPaths);
}

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const QString &pluginSourcePath,
        const QString &pluginDestinationPath,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const bool isAppStoreCompliant,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying plugins from" << pluginSourcePath;

    if (!pluginSourcePath.contains(deploymentInfo.pluginPath))
        return;

    // Plugin white list:
    QStringList pluginList;

    // Platform plugin:
    pluginList.append("platforms/libqcocoa.dylib");

    // Cocoa print support
    pluginList.append("printsupport/libcocoaprintersupport.dylib");

    // Network
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtNetwork.framework"))) {
        QStringList bearerPlugins = QDir(pluginSourcePath +  QStringLiteral("/bearer")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, bearerPlugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("bearer/") + plugin);
        }
    }

    // All image formats (svg if QtSvg.framework is used)
    QStringList imagePlugins = QDir(pluginSourcePath +  QStringLiteral("/imageformats")).entryList(QStringList() << QStringLiteral("*.dylib"));
    foreach (const QString &plugin, imagePlugins) {
        if (plugin.contains(QStringLiteral("qsvg"))) {
            if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtSvg.framework")))
                pluginList.append(QStringLiteral("imageformats/") + plugin);
        } else if (!plugin.endsWith(QStringLiteral("_debug.dylib"))) {
            pluginList.append(QStringLiteral("imageformats/") + plugin);
        }
    }

    // Sql plugins if QtSql.framework is in use
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtSql.framework"))) {
        QStringList sqlPlugins = QDir(pluginSourcePath +  QStringLiteral("/sqldrivers")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, sqlPlugins) {
            if (plugin.endsWith(QStringLiteral("_debug.dylib")))
                continue;

            // Some sql plugins are known to cause app store rejections. Skip or warn for these plugins.
            if (plugin.startsWith(QStringLiteral("libqsqlodbc")) || plugin.startsWith(QStringLiteral("libqsqlpsql"))) {
                Logger::instance().logWarning() << "Plugin" << plugin << "uses private API and is not Mac App store compliant.";
                if (isAppStoreCompliant) {
                    Logger::instance().logWarning() << "Skip plugin" << plugin;
                    continue;
                }
            }

            pluginList.append(QStringLiteral("sqldrivers/") + plugin);
        }
    }

    // multimedia plugins if QtMultimedia.framework is in use
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtMultimedia.framework"))) {
        QStringList plugins = QDir(pluginSourcePath + QStringLiteral("/mediaservice")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, plugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("mediaservice/") + plugin);
        }
        plugins = QDir(pluginSourcePath + QStringLiteral("/audio")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, plugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("audio/") + plugin);
        }
    }

    foreach (const QString &plugin, pluginList) {
        QString sourcePath = pluginSourcePath % QDir::separator() % plugin;
        if (useDebugLibs) {
            // Use debug plugins if found.
            QString debugSourcePath = sourcePath.replace(".dylib", "_debug.dylib");
            if (QFile::exists(debugSourcePath))
                sourcePath = debugSourcePath;
        }

        const QString destinationPath = pluginDestinationPath % QDir::separator() % plugin;
        QDir dir;
        dir.mkpath(QFileInfo(destinationPath).path());

        if (copyFilePrintStatus(sourcePath, destinationPath, alwaysOverwrite)) {
            if (runStrip)
                Deployer::runStrip(destinationPath);
            const ModuleInfo destinationModule(destinationPath);
            const QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo,
                                                                    destinationModule,
                                                                    deploymentInfo.rpathsUsed,
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
            deployQtFrameworks(frameworks,
                               mainModuleInfo,
                               QList<ModuleInfo>() << destinationModule,
                               useDebugLibs,
                               deploymentInfo.useLoaderPath,
                               runStrip,
                               alwaysOverwrite,
                               additionalLibraryPaths);
        }
    }
}

void Deployer::deployQmlImport(
        const ModuleInfo &mainModuleInfo,
        const QSet<QString> &rpaths,
        const QString &importSourcePath,
        const QString &importName,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    const QChar dirSeparator = QDir::separator();
    const QString importDestinationPath = mainModuleInfo.resourcesAbsoluteDirPath()
                                          % dirSeparator % "qml"
                                          % dirSeparator % importName;
    if (QDir().exists(importDestinationPath))
        return;

    recursiveCopyAndDeploy(mainModuleInfo,
                           rpaths,
                           importSourcePath,
                           importDestinationPath,
                           useDebugLibs,
                           useLoaderPath,
                           runStrip,
                           alwaysOverwrite,
                           additionalLibraryPaths);
}

// Scan qml files in qmldirs for import statements, deploy used imports from Qml2ImportsPath to Contents/Resources/qml.
bool Deployer::deployQmlImports(
        const ModuleInfo &mainModuleInfo,
        const DeploymentInfo &deploymentInfo,
        const QStringList &qmlDirs,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    Logger& logger = Logger::instance();
    logger.logInfo() << "";
    logger.logInfo() << "Deploying QML imports ";
    logger.logInfo() << "Application QML file search path(s) is" << qmlDirs.join( ", " );

    // Use qmlimportscanner from QLibraryInfo::BinariesPath
    QString qmlImportScannerPath = QDir::cleanPath(QLibraryInfo::location(QLibraryInfo::BinariesPath) + "/qmlimportscanner");

    // Fallback: Look relative to the macdeployqt binary
    if (!QFile(qmlImportScannerPath).exists())
        qmlImportScannerPath = QCoreApplication::applicationDirPath() + "/qmlimportscanner";

    // Verify that we found a qmlimportscanner binary
    if (!QFile(qmlImportScannerPath).exists()) {
        logger.logError() << "qmlimportscanner not found at" << qmlImportScannerPath;
        logger.logError() << "Rebuild qtdeclarative/tools/qmlimportscanner";
        return false;
    }

    // build argument list for qmlimportsanner: "-rootPath foo/ -rootPath bar/ -importPath path/to/qt/qml"
    // ("rootPath" points to a directory containing app qml, "importPath" is where the Qt imports are installed)
    QStringList argumentList;
    for (const QString &qmlDir : qmlDirs) {
        argumentList.append("-rootPath");
        argumentList.append(qmlDir);
    }

    QString qmlImportsPath = QLibraryInfo::location(QLibraryInfo::Qml2ImportsPath);
    argumentList.append( "-importPath");
    argumentList.append(qmlImportsPath);

    // run qmlimportscanner
    QProcess qmlImportScanner;
    qmlImportScanner.start(qmlImportScannerPath, argumentList);
    if (!qmlImportScanner.waitForStarted()) {
        logger.logError() << "Could not start qmlimpoortscanner. Process error is" << qmlImportScanner.errorString();
        return false;
    }
    qmlImportScanner.waitForFinished();

    // log qmlimportscanner errors
    qmlImportScanner.setReadChannel(QProcess::StandardError);
    QByteArray errors = qmlImportScanner.readAll();
    if (!errors.isEmpty()) {
        logger.logWarning() << "QML file parse error (deployment will continue):";
        logger.logWarning() << errors;
    }

    // parse qmlimportscanner json
    qmlImportScanner.setReadChannel(QProcess::StandardOutput);
    QByteArray json = qmlImportScanner.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(json);
    if (!doc.isArray()) {
        logger.logError() << "qmlimportscanner output error. Expected json array, got:";
        logger.logError() << json;
        return false;
    }

    bool qtQuickContolsInUse = false; // condition for QtQuick.PrivateWidgets below

    // deploy each import
    for (const QJsonValue &importValue : doc.array()) {
        if (!importValue.isObject())
            continue;

        QJsonObject import = importValue.toObject();
        QString name = import["name"].toString();
        QString path = import["path"].toString();
        QString type = import["type"].toString();

        if (import["name"] == "QtQuick.Controls")
            qtQuickContolsInUse = true;

        logger.logInfo() << "Deploying QML import" << name;

        // Skip imports with missing info - path will be empty if the import is not found.
        if (name.isEmpty() || path.isEmpty()) {
            logger.logInfo() << "  Skip import: name or path is empty";
            logger.logInfo() << "";
            continue;
        }

        // Deploy module imports only, skip directory (local/remote) and js imports. These
        // should be deployed as a part of the application build.
        if (type != QStringLiteral("module")) {
            logger.logInfo() << "  Skip non-module import";
            logger.logInfo() << "";
            continue;
        }

        // Create the destination path from the name
        // and version (grabbed from the source path)
        // ### let qmlimportscanner provide this.
        name.replace(QLatin1Char('.'), QLatin1Char('/'));
        int secondTolast = path.length() - 2;
        QString version = path.mid(secondTolast);
        if (version.startsWith(QLatin1Char('.')))
            name.append(version);

        deployQmlImport(mainModuleInfo,
                        deploymentInfo.rpathsUsed,
                        path,
                        name,
                        useDebugLibs,
                        deploymentInfo.useLoaderPath,
                        runStrip,
                        alwaysOverwrite,
                        additionalLibraryPaths);
        logger.logInfo() << "";
    }

    // Special case:
    // Use of QtQuick.PrivateWidgets is not discoverable at deploy-time.
    // Recreate the run-time logic here as best as we can - deploy it iff
    //      1) QtWidgets.framework is used
    //      2) QtQuick.Controls is used
    // The intended failure mode is that libwidgetsplugin.dylib will be present
    // in the app bundle but not used at run-time.
    if (deploymentInfo.deployedFrameworks.contains("QtWidgets.framework") && qtQuickContolsInUse) {
        logger.logInfo() << "Deploying QML import QtQuick.PrivateWidgets";
        QString name = "QtQuick/PrivateWidgets";
        QString path = qmlImportsPath + QLatin1Char('/') + name;
        deployQmlImport(mainModuleInfo,
                        deploymentInfo.rpathsUsed,
                        path,
                        name,
                        useDebugLibs,
                        deploymentInfo.useLoaderPath,
                        runStrip,
                        alwaysOverwrite,
                        additionalLibraryPaths);
        logger.logInfo() << "";

    }
    return true;
}
#else
void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const QString &pluginSourcePath,
        const QString &pluginDestinationPath,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying plugins from" << pluginSourcePath;
    const QStringList plugins = QDir(pluginSourcePath).entryList(QStringList("*." + ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)));

    foreach (const QString &pluginName, plugins) {
        if (pluginSourcePath.contains(deploymentInfo.pluginPath)) {
            QStringList deployedFrameworks = deploymentInfo.deployedFrameworks;

            // Skip the debug versions of the plugins, unless specified otherwise.
            if (!useDebugLibs && pluginName.endsWith("_debug.dylib"))
                continue;

            // Skip the release versions of the plugins, unless specified otherwise.
            if (useDebugLibs && !pluginName.endsWith("_debug.dylib"))
                continue;

            // Skip the qmltooling plugins in release mode or when QtDeclarative is not used.
            if (pluginSourcePath.contains("qmltooling") && (!useDebugLibs || deployedFrameworks.indexOf("QtDeclarative.framework") == -1))
                continue;

            // Skip the designer plugins
            if (pluginSourcePath.contains("plugins/designer"))
                continue;

            // Skip the tracing graphics system
            if (pluginName.contains("libqtracegraphicssystem"))
                continue;

#ifndef QT_GRAPHICSSYSTEM_OPENGL
            // Skip the opengl graphicssystem plugin when not in use.
            if (pluginName.contains("libqglgraphicssystem"))
                continue;
#endif
            // Deploy accessibility for Qt3Support only if the Qt3Support.framework is in use
            if (deployedFrameworks.indexOf("Qt3Support.framework") == -1 && pluginName.contains("accessiblecompatwidgets"))
                continue;

            // Deploy the svg icon plugin if QtSvg.framework is in use.
            if (deployedFrameworks.indexOf("QtSvg.framework") == -1 && pluginName.contains("svg"))
                continue;

            // Deploy the phonon plugins if phonon.framework is in use
            if (deployedFrameworks.indexOf("phonon.framework") == -1 && pluginName.contains("phonon"))
                continue;

            // Deploy the sql plugins if QtSql.framework is in use
            if (deployedFrameworks.indexOf("QtSql.framework") == -1 && pluginName.contains("sql"))
                continue;

            // Deploy the script plugins if QtScript.framework is in use
            if (deployedFrameworks.indexOf("QtScript.framework") == -1 && pluginName.contains("script"))
                continue;

            // Deploy the bearer plugins if QtNetwork.framework is in use
            if (deployedFrameworks.indexOf("QtNetwork.framework") == -1 && pluginName.contains("bearer"))
                continue;
        }

        QDir dir;
        dir.mkpath(pluginDestinationPath);

        const QString sourcePath = pluginSourcePath + QDir::separator() + pluginName;
        const QString destinationPath = pluginDestinationPath + QDir::separator() + pluginName;
        if (copyFilePrintStatus(sourcePath, destinationPath)) {
            if (runStrip)
                Deployer::runStrip(destinationPath);

            // Special case for the phonon plugin: CoreVideo is not available as a separate framework
            // on panther, link against the QuartzCore framework instead. (QuartzCore contians CoreVideo.)
            if (pluginName.contains("libphonon_qt7")) {
                changeInstallName("/System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo",
                        "/System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore",
                        ModuleInfo(destinationPath));
                // TODO: be careful when wrapping the path using ModuleInfo
            }

            // TODO: detect if bundle to wrap the bundle
            const ModuleInfo destinationModule(destinationPath);
            const QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo,
                                                                    destinationModule,
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
            deployQtFrameworks(frameworks,
                               mainModuleInfo,
                               QList<ModuleInfo>() << destinationModule,
                               useDebugLibs,
                               deploymentInfo.useLoaderPath,
                               runStrip,
                               additionalLibraryPaths);
        }
    } // foreach plugins

    QStringList subdirs = QDir(pluginSourcePath).entryList(QStringList() << s_allCharactersAnyCountWildcard, QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString &subdir, subdirs) {
        deployPlugins(mainModuleInfo,
                      pluginSourcePath + QDir::separator() + subdir,
                      pluginDestinationPath + QDir::separator() + subdir,
                      deploymentInfo,
                      useDebugLibs,
                      runStrip,
                      additionalLibraryPaths);
    }
}
#endif

void Deployer::createQtConf(const ModuleInfo &moduleInfo)
{
    if (!moduleInfo.isValid() || !moduleInfo.isBundle())
        return;

    static const QString qtConfFileName = "qt.conf";
    static const QByteArray qtConfFileContents = "[Paths]\nPlugins = PlugIns\n";
    const QString filePath = moduleInfo.resourcesAbsoluteDirPath();
    const QString fileName = filePath
                             + QDir::separator()
                             + qtConfFileName;

    QDir().mkpath(filePath);

    QFile qtconf(fileName);
    if (qtconf.exists()) {
        Logger::instance().logWarning()
                << fileName << "already exists, will not overwrite." << endl
                << "To make sure the plugins are loaded from the correct location," << endl
                << "please make sure qt.conf contains the following lines:" << endl
                << "[Paths]" << endl
                << "  Plugins = PlugIns";
        return;
    }

    if (qtconf.open(QIODevice::WriteOnly)) {
        if (qtconf.write(qtConfFileContents) != -1) {
            Logger::instance().logInfo()
                    << "Created configuration file:" << fileName << endl
                    << "This file sets the plugin search path to" << filePath;
        }
    } else {
        Logger::instance().logError() << "Failed to open the file: \"" << fileName << "\" for writing.";
    }
}

void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const bool isAppStoreCompliant,
        const QStringList &additionalLibraryPaths)
{
    if (!mainModuleInfo.isValid())
        return;

    const QString pluginDestinationPath = mainModuleInfo.plugInsAbsoluteDirPath();
    deployPlugins(mainModuleInfo,
                  deploymentInfo.pluginPath,
                  pluginDestinationPath,
                  deploymentInfo,
                  useDebugLibs,
                  runStrip,
                  alwaysOverwrite,
                  isAppStoreCompliant,
                  additionalLibraryPaths);
}

void Deployer::changeQtFrameworks(
        const QList<FrameworkInfo> &frameworks,
        const QList<ModuleInfo> &modules,
        const QString &qtPath)
{
    Logger::instance().logInfo()
            << "Changing" << modules << "to link against" << endl
            << "Qt in" << qtPath;
    QString finalQtPath = qtPath;

    if (!qtPath.startsWith("/Library/Frameworks"))
        finalQtPath += "/lib/";

    for (const FrameworkInfo &framework : frameworks) {
        const QString oldBinaryId = framework.installName;
        const QString newBinaryId = finalQtPath % framework.frameworkName % framework.binaryPath;
        for (const ModuleInfo &module : modules) {
            changeInstallName(oldBinaryId, newBinaryId, module);
        }
    }
}

void Deployer::codesignFile(const QString &identity, const QString &filePath)
{
    Logger& logger = Logger::instance();
    logger.logInfo() << "codesign" << filePath;

    QProcess codesign;
    codesign.start("codesign", QStringList() << "--preserve-metadata=identifier,entitlements"
                                             << "--force" << "-s" << identity << filePath);
    codesign.waitForFinished(-1);

    QByteArray err = codesign.readAllStandardError();
    if (codesign.exitCode() > 0) {
        logger.logError() << "Codesign signing error:";
        logger.logError() << err;
    } else if (!err.isEmpty()) {
        logger.logDebug() << err;
    }
}

QStringList Deployer::findAppBundleFiles(const QString &appBundlePath)
{
    QStringList result;

    QDirIterator iter(appBundlePath, QStringList() << s_allCharactersAnyCountWildcard,
            QDir::Files, QDirIterator::Subdirectories);

    while (iter.hasNext()) {
        iter.next();
        if (iter.fileInfo().isSymLink())
            continue;
        result << iter.fileInfo().filePath();
    }

    return result;
}

QSet<QString> Deployer::codesignBundle(const ModuleInfo &moduleInfo, const QString &identity)
{
    if ( ! moduleInfo.isValid()
         || identity.isEmpty() )
    {
        return QSet<QString>();
    }

    // Code sign all binaries in the app bundle. This needs to
    // be done inside-out, e.g sign framework dependencies
    // before the main app binary. The codesign tool itself has
    // a "--deep" option to do this, but usage when signing is
    // not recommended: "Signing with --deep is for emergency
    // repairs and temporary adjustments only."

    const QString modulePath = moduleInfo.path();
    Logger::instance().logInfo() << "";
    Logger::instance().logInfo() << "Signing" << moduleInfo.path() << "with identity" << identity;

    QStack<QString> pendingBinaries;
    QSet<QString> signedBinaries;

    // Create the root code-binary set. This set consists of the application
    // executable(s) and the plugins.
    const QChar dirSeparator = QDir::separator();
    QString rootBinariesPath = QFileInfo( moduleInfo.binaryFilePath() ).canonicalPath() + dirSeparator;
    QStringList foundRootBinaries = QDir(rootBinariesPath).entryList(QStringList() << s_allCharactersAnyCountWildcard, QDir::Files);
    foreach (const QString &binary, foundRootBinaries)
        pendingBinaries.push(rootBinariesPath + binary);

    QStringList foundPluginBinaries = findAppBundleFiles(moduleInfo.plugInsAbsoluteDirPath() + dirSeparator);
    foreach (const QString &binary, foundPluginBinaries)
         pendingBinaries.push(binary);

    // Add frameworks for processing.
    const QStringList frameworkPaths = moduleInfo.frameworksPaths();
    for (const QString &frameworkPath : frameworkPaths) {

        // Add all files for a framework as a catch all.
        QStringList bundleFiles = findAppBundleFiles(frameworkPath);
        foreach (const QString &binary, bundleFiles)
            pendingBinaries.push(binary);

        // Prioritise first to sign any additional inner bundles found in the Helpers folder (e.g
        // used by QtWebEngine).
        QDirIterator helpersIterator(frameworkPath,
                                     QStringList() << QString::fromLatin1("Helpers"),
                                     QDir::Dirs | QDir::NoSymLinks,
                                     QDirIterator::Subdirectories);
        while (helpersIterator.hasNext()) {
            helpersIterator.next();
            QString helpersPath = helpersIterator.filePath();
            QStringList innerBundleNames = QDir(helpersPath).entryList(QStringList()
                                                                       << ( s_allCharactersAnyCountWildcard % '.'
                                                                            % ModuleInfo::moduleExtension( ModuleTypeApplicationBundle ) ),
                                                                       QDir::Dirs);
            for (const QString &innerBundleName : innerBundleNames)
                signedBinaries += codesignBundle( ModuleInfo( helpersPath % dirSeparator % innerBundleName ), identity );
        }

        // Also make sure to sign any libraries that will not be found by otool because they
        // are not linked and won't be seen as a dependency.
        QDirIterator librariesIterator(frameworkPath, QStringList() << QString::fromLatin1("Libraries"), QDir::Dirs | QDir::NoSymLinks, QDirIterator::Subdirectories);
        while (librariesIterator.hasNext()) {
            librariesIterator.next();
            QString librariesPath = librariesIterator.filePath();
            bundleFiles = findAppBundleFiles(librariesPath);
            for (const QString &binary : bundleFiles)
                pendingBinaries.push(binary);
        }
    }

    // Sign all binares; use otool to find and sign dependencies first.
    while (!pendingBinaries.isEmpty()) {
        QString binary = pendingBinaries.pop();
        if (signedBinaries.contains(binary))
            continue;

        // Check if there are unsigned dependencies, sign these first
        QStringList dependencies = getBinaryDependencies(rootBinariesPath, binary).toSet().subtract(signedBinaries).toList();

        if (!dependencies.isEmpty()) {
            pendingBinaries.push(binary);
            int dependenciesSkipped = 0;
            for (const QString &dependency : dependencies) {
                // Skip dependencies that are outside the current app bundle, because this might
                // cause a codesign error if the current bundle is part of the dependency (e.g.
                // a bundle is part of a framework helper, and depends on that framework).
                // The dependencies will be taken care of after the current bundle is signed.
                if (!dependency.startsWith(modulePath)) {
                    ++dependenciesSkipped;
                    Logger::instance().logInfo() << "Skipping outside dependency: " << dependency;
                    continue;
                }
                pendingBinaries.push(dependency);
            }

            // If all dependencies were skipped, make sure the binary is actually signed, instead
            // of going into an infinite loop.
            if (dependenciesSkipped == dependencies.size()) {
                pendingBinaries.pop();
            } else {
                continue;
            }
        }

        // All dependencies are signed, now sign this binary
        codesignFile(identity, binary);
        signedBinaries.insert(binary);
    }

    Logger::instance().logInfo() << "Finished codesigning " << modulePath << "with identity" << identity;

    // Verify code signature
    QProcess codesign;
    codesign.start("codesign", QStringList() << "--deep" << "-v" << modulePath);
    codesign.waitForFinished(-1);
    QByteArray err = codesign.readAllStandardError();
    if (codesign.exitCode() > 0) {
        Logger::instance().logError() << "codesign verification error:";
        Logger::instance().logError() << err;
    } else if (!err.isEmpty()) {
        Logger::instance().logDebug() << err;
    }

    return signedBinaries;
}

void Deployer::codesign(const ModuleInfo &moduleInfo, const QString &identity) {
    codesignBundle(moduleInfo, identity);
}

void Deployer::createDiskImage(const ModuleInfo &moduleInfo, const bool alwaysOverwrite)
{
    if (!moduleInfo.isValid())
        return;

    const QFileInfo moduleFileInfo(moduleInfo.path());
    const QString moduleBaseName = moduleFileInfo.completeBaseName();
    const QString dmgFilePath = moduleBaseName + ".dmg";

    QFile dmgFile(dmgFilePath);
    if (dmgFile.exists() && alwaysOverwrite)
        dmgFile.remove();

    if (dmgFile.exists()) {
        Logger::instance().logInfo() << "Disk image already exists, skipping .dmg creation for" << dmgFile.fileName();
        return;
    }

    Logger::instance().logInfo() << "Creating disk image (.dmg) for" << moduleInfo.path();

    // More dmg options can be found in the hdiutil man page.
    const QStringList options = QStringList()
            << "create" << dmgFilePath
            << "-srcfolder" << moduleInfo.path()
            << "-format" << "UDZO"
            << "-volname" << moduleBaseName;

    QProcess hdutil;
    hdutil.start("hdiutil", options);
    hdutil.waitForFinished(-1);
}

QStringList Deployer::getBinaryDependencies(const QString& executablePath, const QString &path)
{
    QStringList binaries;

    QStringList outputLines;
    QString errorMessage;
    if ( ! Deployer::binarySharedLibraries( path, & outputLines, & errorMessage ) )
    {
        Logger::instance().logError() << errorMessage;
    }

    outputLines.removeFirst(); // remove line containing the binary path

    bool rpathsLoaded = false;
    QSet<QString> rpaths;

    const QChar dirSeparator = QDir::separator();
    const QString executablePathSlashed = ModuleInfo::exePath + dirSeparator;
    const QString rPathSlashed = ModuleInfo::rPath + dirSeparator;

    // return bundle-local dependencies. (those starting with @executable_path)
    foreach (const QString &line, outputLines) {
        QString trimmedLine = line.mid(0, line.indexOf("(")).trimmed(); // remove "(compatibility version ...)" and whitespace
        if (trimmedLine.startsWith(executablePathSlashed)) {
            QString binary = QDir::cleanPath(executablePath + trimmedLine.mid(executablePathSlashed.length()));
            if (binary != path)
                binaries.append(binary);
        } else if (trimmedLine.startsWith(rPathSlashed)) {
            if (!rpathsLoaded) {
                rpaths = getBinaryRPaths(path, true, executablePath);
                rpathsLoaded = true;
            }
            bool resolved = false;
            foreach (const QString &rpath, rpaths) {
                QString binary = QDir::cleanPath(rpath % dirSeparator % trimmedLine.mid(rPathSlashed.length()));
                Logger::instance().logDebug() << "Checking for" << binary;
                if (QFile::exists(binary)) {
                    binaries.append(binary);
                    resolved = true;
                    break;
                }
            }
            if (!resolved && !rpaths.isEmpty()) {
                Logger::instance().logError() << "Cannot resolve rpath" << trimmedLine;
                Logger::instance().logError() << " using" << rpaths.toList().join( ", " );
            }
        }
    }

    return binaries;
}

void Deployer::stripModuleBinary(const ModuleInfo& moduleInfo)
{
    runStrip(moduleInfo.binaryFilePath());
}

bool Deployer::syncExecuteTool(
        const QString& aToolName,
        const QStringList& aArgs,
        QString* const aOutput,
        QString* const aErrorOutput)
{
    if ( aToolName.isEmpty() )
    {
        return false;
    }

    QProcess process;
    process.start(aToolName, aArgs);
    process.waitForFinished();

    const bool finishedSuccessfully = process.exitStatus() == QProcess::NormalExit
                                      && process.exitCode() == EXIT_SUCCESS;
    if (!finishedSuccessfully)
    {
        if ( aErrorOutput != nullptr )
        {
            *aErrorOutput = process.readAllStandardError();
        }
    }

    if (aOutput != nullptr)
    {
        *aOutput = process.readAllStandardOutput();
    }

    return finishedSuccessfully;
}

bool Deployer::runOtool(
        const QStringList& aArgs,
        QStringList* const aOutput,
        QString* const aErrorOutput)
{
    QString output;
    const bool wasExecutionSuccessful = Deployer::syncExecuteTool("otool", aArgs, & output, aErrorOutput );
    *aOutput = output.split( Utilities::newline() );

    return wasExecutionSuccessful;
}

bool Deployer::binaryLoadCommands(
        const QString& aBinaryFilePath,
        QStringList* const aOutput,
        QString* const aErrorOutput)
{
    return Deployer::runOtool( QStringList{ "-l" } << aBinaryFilePath, aOutput, aErrorOutput );
}

bool Deployer::binarySharedLibraries(
        const QString& aBinaryFilePath,
        QStringList* const aOutput,
        QString* const aErrorOutput)
{
    return Deployer::runOtool( QStringList{ "-L" } << aBinaryFilePath, aOutput, aErrorOutput );
}


MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream &stream, const FrameworkInfo &info)
{
    stream << "Framework name: " << info.frameworkName << endl
           << "Framework directory: " << info.frameworkDirectory << endl
           << "Framework path: " << info.frameworkPath << endl
           << "Binary directory: " << info.binaryDirectory << endl
           << "Binary name: " << info.binaryName << endl
           << "Binary path: " << info.binaryPath << endl
           << "Version: " << info.version << endl
           << "Install name: " << info.installName << endl
           << "Deployed install name: " << info.deployedInstallName << endl
           << "Framework Destination Directory (relative to bundle)" << info.frameworkDestinationDirectory << endl
           << "Binary Destination Directory (relative to bundle)" << info.binaryDestinationDirectory << endl;

    return stream;
}
