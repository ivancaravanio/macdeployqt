INCLUDEPATH += $${PWD}

SOURCES += \
    $${PWD}/DeploymentSettings.cpp \
    $${PWD}/DeploymentSettingsParser.cpp \
    $${PWD}/abstractloggerstream.cpp \
    $${PWD}/deployer.cpp \
    $${PWD}/logger.cpp \
    $${PWD}/moduleinfo.cpp \
    $${PWD}/Utilities.cpp

HEADERS += \
    $${PWD}/DeploymentSettings.h \
    $${PWD}/DeploymentSettingsParser.h \
    $${PWD}/abstractloggerstream.h \
    $${PWD}/deployer.h \
    $${PWD}/logger.h \
    $${PWD}/moduleinfo.h \
    $${PWD}/Utilities.h

