/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGS_H
#define MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGS_H

#include "logger.h"
#include "moduleinfo.h"

#include <QDebug>
#include <QList>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QCoreApplication>

namespace MacDeployQt {
namespace Shared {

class DeploymentSettings
{
public:
    static const VerbosityLevel s_logVerbosityLevelDefault;
    static const bool           s_shouldDeployPluginsDefault;
    static const bool           s_shouldCreateDmgDefault;
    static const bool           s_shouldUseDebugLibsDefault;
    static const bool           s_shouldRunStripDefault;
    static const QString        s_qmlFilesDirPathDefault;
    static const bool           s_alwaysOverwriteDefault;
    static const QString        s_codeSignIdentityDefault;
    static const bool           s_isAppStoreCompliantDefault;
    static const QStringList    s_additionalLibrarySearchDirPathsDefault;

public:
    DeploymentSettings();
    explicit DeploymentSettings(const ModuleInfo& aDeploymentTarget);
    explicit DeploymentSettings(const QCoreApplication& aApp);
    explicit DeploymentSettings(const QStringList& aArguments);

    const ModuleInfo& deploymentTarget() const;

    void setLogVerbosityLevel( const VerbosityLevel aLogVerbosityLevel );
    VerbosityLevel logVerbosityLevel() const;

    void setShouldDeployPlugins( const bool aShouldDeployPlugins );
    bool shouldDeployPlugins() const;

    void setShouldCreateDmg( const bool aShouldCreateDmg );
    bool shouldCreateDmg() const;

    void setUseDebugLibs( const bool aShouldUseDebugLibs );
    bool shouldUseDebugLibs() const;

    void setShouldRunStrip( const bool aShouldRunStrip );
    bool shouldRunStrip() const;

    void setAdditionalExecutables(const QList<ModuleInfo>& aAdditionalExecutables);
    QList<ModuleInfo> additionalExecutables() const;

    void setQmlFilesDirPath(const QString& aQmlFilesDirPath);
    QString qmlFilesDirPath() const;
    static bool isQmlFilesDirPathValid(const QString& aQmlFilesDirPath);

    void setAlwaysOverwrite(const bool aAlwaysOverwrite);
    bool alwaysOverwrite() const;

    void setCodeSignIdentity(const QString& aCodeSignIdentity);
    QString codeSignIdentity() const;
    bool shouldCodeSign() const;

    void setIsAppStoreCompliant(const bool aIsAppStoreCompliant);
    bool isAppStoreCompliant() const;

    void setAdditionalLibrarySearchDirPaths(const QStringList& aAdditionalLibrarySearchDirPaths);
    QStringList additionalLibrarySearchDirPaths() const;

    bool load( const QCoreApplication& aApp, QString* const aErrorText = nullptr );
    bool load( const QStringList& aArguments, QString* const aErrorText = nullptr );

    bool isValid() const;

private:
    void setDeploymentTarget(const ModuleInfo& aDeploymentTarget);
    static bool isDirPathIsEmptyOrExists(const QString& aDirPath);

private:
    ModuleInfo        m_deploymentTarget;
    VerbosityLevel    m_logVerbosityLevel;
    bool              m_shouldDeployPlugins;
    bool              m_shouldCreateDmg;
    bool              m_shouldUseDebugLibs;
    bool              m_shouldRunStrip;
    QList<ModuleInfo> m_additionalExecutables;
    QString           m_qmlFilesDirPath;
    bool              m_alwaysOverwrite;
    QString           m_codeSignIdentity;
    bool              m_isAppStoreCompliant;
    QStringList       m_additionalLibrarySearchDirPaths;
};

}
}

#endif // MACDEPLOYQT_SHARED_DEPLOYMENTSETTINGS_H
