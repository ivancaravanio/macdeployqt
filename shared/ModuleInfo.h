/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_MODULEINFO_H
#define MACDEPLOYQT_SHARED_MODULEINFO_H

#include "abstractloggerstream.h"

#include <QList>
#include <QString>
#include <QStringList>

namespace MacDeployQt {
namespace Shared {

enum ModuleType
{
    ModuleTypeUnknown,
    ModuleTypeApplicationBundle,
    ModuleTypeApplicationExecutable,
    ModuleTypeLibraryBundle,
    ModuleTypeLibraryDynamicBinary,
    ModuleTypeLibraryStaticBinary
};

enum BundleType
{
    BundleTypeApplication,
    BundleTypeLibrary
};

class ModuleInfo
{
public:
    ModuleInfo();
    explicit ModuleInfo(const QString &path);
    ModuleInfo(const ModuleInfo &moduleInfo);
    ~ModuleInfo();

    bool isValid() const;

    ModuleType type() const;
    bool isBundle() const;
    bool isBinary() const;
    bool isApplication() const;
    bool isLibrary() const;

    const QString &path() const;
    const QString &binaryFilePath() const;
    QString calculatedBinaryFilePath() const;

    // module type == binary => the directory that contains the binary
    // module type == bundle => the directory that contains the bundle plus the bundle's name
    QString workingDirPath() const;

    QString bundleRelativeBinaryFilePath() const;
    QString bundleRelativeBinaryDirPath() const;

    // module type == binary => dot (.)
    // module type == bundle => a path relative to the bundle (bundleName.bundleExt/[this-path-here])
    QString dependenciesRelativeDirPath() const;
    QString dependenciesAbsoluteDirPath() const;
    QString librariesInstallNameDirPath() const;

    QString librariesRelativeDirPath() const;
    QString librariesAbsoluteDirPath() const;

    QString plugInsRelativeDirPath() const;
    QString plugInsAbsoluteDirPath() const;

    QString resourcesRelativeDirPath() const;
    QString resourcesAbsoluteDirPath() const;

    QStringList librariesFilePaths( const bool aShouldCollectNamesOnly = false ) const;
    QStringList frameworksPaths( const bool aShouldCollectNamesOnly = false ) const;

    const QList<ModuleInfo> &bundleLibraryModules() const;

    static const QString &moduleExtension(const ModuleType moduleType);

    bool operator==(const ModuleInfo &other) const;
    bool operator!=(const ModuleInfo &other) const;

public:
    static const QString contentsDirName;
    static const QString resourcesDirName;
    static const QString frameworksDirName;
    static const QString librariesDirName;
    static const QString pluginsDirName;
    static const QString versionsDirName;
    static const QString currentVersionDirName;
    static const QString helpersDirName;

    static const QString exePath;
    static const QString loaderPath;
    static const QString rPath;

    static const QString informationPropertyListFileName;

private:
    static int machoHeaderFileType(const QString &filePath);
    void findBundleLibraryFilePaths(const QString &path);
    static bool verifyInformationPropertyList(
            const QString &infoPlistFilePath,
            const BundleType expectedBundleType,
            const QString &expectedBundleBinaryFileName);
    bool basicDataEquals(const ModuleInfo &other) const;

private:
    ModuleType        m_type;
    QString           m_binaryFilePath;
    QString           m_path;
    QList<ModuleInfo> m_bundleLibraryModules;
};

}
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream& stream, const MacDeployQt::Shared::ModuleInfo &info);
MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream& stream, const QList<MacDeployQt::Shared::ModuleInfo> &moduleInfos);

#endif // MACDEPLOYQT_SHARED_MODULEINFO_H
