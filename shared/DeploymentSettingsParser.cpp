/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "DeploymentSettingsParser.h"

#include "Utilities.h"

#include <QCommandLineParser>
#include <QDebug>
#include <QDir>
#include <QHash>
#include <QObject>
#include <QStringBuilder>

using namespace MacDeployQt::Shared;

DeploymentSettings DeploymentSettingsParser::deploymentSettings(
        const QStringList& aArguments,
        QString* const aErrorText )
{
    const QScopedPointer< QCommandLineParser > parser{ DeploymentSettingsParser::parser() };
    parser->process( aArguments );

    return DeploymentSettingsParser::deploymentSettings( parser.data(), aErrorText );
}

DeploymentSettings DeploymentSettingsParser::deploymentSettings(
        const QCoreApplication& aApp,
        QString* const aErrorText )
{
    const QScopedPointer< QCommandLineParser > parser{ DeploymentSettingsParser::parser() };
    parser->process( aApp );

    return DeploymentSettingsParser::deploymentSettings( parser.data(), aErrorText );
}

QCommandLineParser* DeploymentSettingsParser::parser()
{
    QCommandLineParser* const cmdLineParser = new QCommandLineParser;
    cmdLineParser->setSingleDashWordOptionMode( QCommandLineParser::ParseAsLongOptions );
    cmdLineParser->setApplicationDescription( DeploymentSettingsParser::appDescription() );
    // cmdLineParser->setOptionsAfterPositionalArgumentsMode();

    cmdLineParser->addPositionalArgument( "target",
                                          QObject::tr( "The deployment target. A application bundle (.app) / "
                                                       "library bundle (.framework) / "
                                                       "bundle (.bundle) / "
                                                       "console application (extensionless) / "
                                                       "dynamic library (.dyld)" ) );

    for ( int i = 0; i < OptionArgumentTypesCount; ++ i )
    {
        const OptionArgumentType optionArgumentType = static_cast< OptionArgumentType >( i );
        const QCommandLineOption option(
                    DeploymentSettingsParser::optionArgumentName( optionArgumentType ),
                    DeploymentSettingsParser::optionArgumentDescription( optionArgumentType ),
                    DeploymentSettingsParser::optionArgumentValueName( optionArgumentType ) );
        if ( ! cmdLineParser->addOption( option ) )
        {
            qDebug() << "option added unsuccessfully";
        }
    }

    cmdLineParser->addHelpOption();
    cmdLineParser->addVersionOption();

    return cmdLineParser;
}

QString DeploymentSettingsParser::appDescription()
{
    return QObject::tr( "macdeployqt takes an:%1"
                        "\t1/ application bundle (.app)%1"
                        "\t2/ library bundle (.framework)%1"
                        "\t3/ bundle (.bundle)%1"
                        "\t4/ console application (extensionless)%1"
                        "\t5/ dynamic library (.dyld)%1"
                        "as input and makes it self-contained by copying in%1"
                        "the Qt frameworks and plugins that the application uses.%1"
                        "%1"
                        "Plugins related to a framework are copied in with the%1"
                        "framework. The accessibilty, image formats, and text codec%1"
                        "plugins are always copied, unless \"-no-plugins\" is specified.%1"
                        "%1"
                        "Qt plugins may use private API and will cause the app to be%1"
                        "rejected from the Mac App store. MacDeployQt will print a warning%1"
                        "when known incompatible plugins are deployed. Use -appstore-compliant%1"
                        "to skip these plugins. Currently two SQL plugins are known to%1"
                        "be incompatible: qsqlodbc and qsqlpsql.%1"
                        "%1"
                        "See the \"Deploying an Application on Qt/Mac\" topic in the%1"
                        "documentation for more information about deployment on Mac OS X." )
           .arg( Utilities::newline() );
}

QString DeploymentSettingsParser::optionArgumentName(
        const OptionArgumentType aOptionArgumentType,
        const bool aShouldDecorate )
{
    QString argumentName;
    switch ( aOptionArgumentType )
    {
        case OptionArgumentTypeNoPlugins:                       argumentName = "no-plugins"; break;
        case OptionArgumentTypeDMG:                             argumentName = "dmg"; break;
        case OptionArgumentTypeNoStrip:                         argumentName = "no-strip"; break;
        case OptionArgumentTypeUseDebugLibs:                    argumentName = "use-debug-libs"; break;
        case OptionArgumentTypeLogVerbosity:                    argumentName = "verbose"; break;
        case OptionArgumentTypeExecutable:                      argumentName = "executable"; break;
        case OptionArgumentTypeQmlDir:                          argumentName = "qmldir"; break;
        case OptionArgumentTypeAlwaysOverwrite:                 argumentName = "always-overwrite"; break;
        case OptionArgumentTypeCodeSignIdentity:                argumentName = "codesign"; break;
        case OptionArgumentTypeIsAppstoreCompliant:             argumentName = "appstore-compliant"; break;
        case OptionArgumentTypeAdditionalLibrarySearchDirPaths: argumentName = "add-lib-dir-paths"; break;
        default:
            return QString();
    }

    return aShouldDecorate
           ? '-' + argumentName
           : argumentName;
}

DeploymentSettingsParser::OptionArgumentType DeploymentSettingsParser::optionArgumentType( const QString& aOptionArgumentName )
{
    static QHash< QString, OptionArgumentType > optionArgumentNameToTypeMappings;
    if ( optionArgumentNameToTypeMappings.isEmpty() )
    {
        optionArgumentNameToTypeMappings.reserve( OptionArgumentTypesCount );
        for ( int i = 0; i < OptionArgumentTypesCount; ++ i )
        {
            const OptionArgumentType optionArgumentType = static_cast< OptionArgumentType >( i );
            optionArgumentNameToTypeMappings.insert(
                        DeploymentSettingsParser::optionArgumentName( optionArgumentType ),
                        optionArgumentType );
        }
    }

    return optionArgumentNameToTypeMappings.value( aOptionArgumentName, OptionArgumentTypeUnknown );

}

QString DeploymentSettingsParser::optionArgumentValueName(
        const OptionArgumentType aOptionArgumentType,
        const bool aShouldDecorate )
{
    QString valueName;
    switch ( aOptionArgumentType )
    {
        case OptionArgumentTypeLogVerbosity:                    valueName = "level"; break;
        case OptionArgumentTypeExecutable:                      valueName = "file-path"; break;
        case OptionArgumentTypeQmlDir:                          valueName = "dir-path"; break;
        case OptionArgumentTypeCodeSignIdentity:                valueName = "identity"; break;
        case OptionArgumentTypeAdditionalLibrarySearchDirPaths: valueName = "dir-paths"; break;
        default:
            return QString();
    }

    return aShouldDecorate
           ? '<' % valueName % '>'
           : valueName;
}

QString DeploymentSettingsParser::optionArgumentDescription( const OptionArgumentType aOptionArgumentType )
{
    switch ( aOptionArgumentType )
    {
        case OptionArgumentTypeNoPlugins:
            return QObject::tr( "Skip plugin deployment." );
        case OptionArgumentTypeDMG:
            return QObject::tr( "Create a .dmg disk image." );
        case OptionArgumentTypeNoStrip:
            return QObject::tr( "Don\'t run \'strip\' on the binaries." );
        case OptionArgumentTypeUseDebugLibs:
            return QObject::tr( "Deploy with debug versions of frameworks and plugins (implies %1)." )
                    .arg( DeploymentSettingsParser::optionArgumentName( OptionArgumentTypeNoStrip, true ) );
        case OptionArgumentTypeLogVerbosity:
            return QObject::tr( "The level of detail of the logs. Possible values are:%1 "
                                "0 - no output%1 "
                                "1 - errors%1 "
                                "2 - errors + warnings (default)%1 "
                                "3 - errors + warnings + info%1 "
                                "4 - errors + warnings + info + debug" )
                    .arg( Utilities::newline() );
        case OptionArgumentTypeExecutable:
            return QObject::tr( "Let the given executable use the deployed frameworks too." );
        case OptionArgumentTypeQmlDir:
            return QObject::tr( "Deploy imports used by .qml files in the given %1." )
                    .arg( DeploymentSettingsParser::optionArgumentValueName( aOptionArgumentType, true ) );
        case OptionArgumentTypeAlwaysOverwrite:
            return QObject::tr( "Copy files even if the target file exists." );
        case OptionArgumentTypeCodeSignIdentity:
            return QObject::tr( "Run codesign with the given %1 on all executables." )
                    .arg( DeploymentSettingsParser::optionArgumentValueName( aOptionArgumentType, true ) );
        case OptionArgumentTypeIsAppstoreCompliant:
            return QObject::tr( "Skip deployment of components that use private API." );
        case OptionArgumentTypeAdditionalLibrarySearchDirPaths:
            return QObject::tr( "Specify additional paths where macdeployqt will search for dependent libraries\' binaries. "
                                "The paths should be separated using semicolon (;):%1 "
                                "%2=<path-1>;<path-2>%1 "
                                "If either of the paths contains whitespace, it should be escaped "
                                "by prepending a backward slash (\\) to the whitespace character. "
                                "The tool allows for specifying numerous %2 switches." )
                    .arg( Utilities::newline() )
                    .arg( DeploymentSettingsParser::optionArgumentName( aOptionArgumentType, true ) );
        default:
            break;
    }

    return QString();
}

bool DeploymentSettingsParser::isOptionArgumentTypeValid( const DeploymentSettingsParser::OptionArgumentType aOptionArgumentType )
{
    return aOptionArgumentType == OptionArgumentTypeNoPlugins
           || aOptionArgumentType == OptionArgumentTypeDMG
           || aOptionArgumentType == OptionArgumentTypeNoStrip
           || aOptionArgumentType == OptionArgumentTypeUseDebugLibs
           || aOptionArgumentType == OptionArgumentTypeLogVerbosity
           || aOptionArgumentType == OptionArgumentTypeExecutable
           || aOptionArgumentType == OptionArgumentTypeQmlDir
           || aOptionArgumentType == OptionArgumentTypeAlwaysOverwrite
           || aOptionArgumentType == OptionArgumentTypeCodeSignIdentity
           || aOptionArgumentType == OptionArgumentTypeIsAppstoreCompliant
           || aOptionArgumentType == OptionArgumentTypeAdditionalLibrarySearchDirPaths;
}

DeploymentSettings DeploymentSettingsParser::deploymentSettings(
        QCommandLineParser* const aParser,
        QString* const errorText )
{
    if ( aParser == nullptr )
    {
        return DeploymentSettings();
    }

    const QStringList deploymentTargetPathCandidates = aParser->positionalArguments();
    const int deploymentTargetPathCandidatesCount = deploymentTargetPathCandidates.size();
    if ( deploymentTargetPathCandidatesCount == 0 )
    {
        if ( errorText != nullptr )
        {
            *errorText = DeploymentSettingsParser::errorText( aParser,
                                                              QObject::tr( "Dependency target not specified." ) );
        }

        return DeploymentSettings{};
    }
    else if ( deploymentTargetPathCandidatesCount > 1 )
    {
        if ( errorText != nullptr )
        {
            *errorText = DeploymentSettingsParser::errorText( aParser,
                                                              QObject::tr( "Too many dependency targets specified." ) );
        }

        return DeploymentSettings{};
    }

    const ModuleInfo deploymentTarget{ deploymentTargetPathCandidates.first() };
    if ( ! deploymentTarget.isValid() )
    {
        if ( errorText != nullptr )
        {
            *errorText = DeploymentSettingsParser::errorText( aParser,
                                                              QObject::tr( "Deployment target either doesn\'t exist or is of invalid type." ) );
        }

        return DeploymentSettings{};
    }

    const QStringList unknownOptionArgumentNames = aParser->unknownOptionNames();
    if ( ! unknownOptionArgumentNames.isEmpty() )
    {
        if ( errorText != nullptr )
        {
            *errorText = DeploymentSettingsParser::errorText( aParser,
                                                              QObject::tr( "Uknown option arguments detected: %1" )
                                                              .arg( unknownOptionArgumentNames.join( ", " ) ) );
        }

        return DeploymentSettings{};
    }

    DeploymentSettings deploymentSettings{ deploymentTarget };

    QList< ModuleInfo > additionalExecutables;
    QStringList additionalLibrarySearchPaths;

    const QStringList optionArgumentNames = aParser->optionNames();
    for ( const QString& optionArgumentName : optionArgumentNames )
    {
        const OptionArgumentType optionArgumentType = DeploymentSettingsParser::optionArgumentType( optionArgumentName );
        if ( ! DeploymentSettingsParser::isOptionArgumentTypeValid( optionArgumentType ) )
        {
            return DeploymentSettings{};
        }

        switch ( optionArgumentType )
        {
            case OptionArgumentTypeNoPlugins: deploymentSettings.setShouldDeployPlugins( false ); break;
            case OptionArgumentTypeDMG: deploymentSettings.setShouldCreateDmg( true ); break;
            case OptionArgumentTypeNoStrip: deploymentSettings.setShouldRunStrip( false ); break;
            case OptionArgumentTypeUseDebugLibs: deploymentSettings.setUseDebugLibs( true ); break;
            case OptionArgumentTypeLogVerbosity:
            {
                bool converted = false;
                const VerbosityLevel verbosityLevel = static_cast< VerbosityLevel >( aParser->value( optionArgumentName ).toInt( & converted ) );
                if ( ! converted || ! Logger::isVerbosityLevelValid( verbosityLevel ) )
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "Incorrectly formatted or unsupported log verbosity level." ) );
                    }

                    return DeploymentSettings{};
                }

                deploymentSettings.setLogVerbosityLevel( verbosityLevel );
                break;
            }
            case OptionArgumentTypeExecutable:
            {
                const QString executableFilePath = aParser->value( optionArgumentName );
                if ( executableFilePath.isEmpty() )
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "Missing executable path." ) );
                    }

                    return DeploymentSettings{};
                }

                const ModuleInfo additionaExecutableInfo( executableFilePath );
                if (additionaExecutableInfo.type() != ModuleTypeApplicationExecutable)
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "The provided additional executable file path does not point to a valid executable file." ) );
                    }

                    return DeploymentSettings{};
                }

                additionalExecutables.append(additionaExecutableInfo);
                break;
            }
            case OptionArgumentTypeQmlDir:
            {
                const QString qmlDirPath = aParser->value( optionArgumentName );
                if ( qmlDirPath.isEmpty() )
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "Missing qml dir path." ) );
                    }

                    return DeploymentSettings{};
                }

                if ( ! QDir( qmlDirPath ).exists() )
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "The qml dir path doesn\'t exist." ) );
                    }

                    return DeploymentSettings{};
                }

                deploymentSettings.setQmlFilesDirPath(qmlDirPath);
                break;
            }
            case OptionArgumentTypeAlwaysOverwrite: deploymentSettings.setAlwaysOverwrite( true ); break;
            case OptionArgumentTypeCodeSignIdentity:
            {
                const QString codeSignIdentity = aParser->value( optionArgumentName );
                if ( codeSignIdentity.isEmpty() )
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "Missing code sign identity." ) );
                    }

                    return DeploymentSettings{};
                }

                deploymentSettings.setCodeSignIdentity( codeSignIdentity );
                break;
            }
            case OptionArgumentTypeIsAppstoreCompliant: deploymentSettings.setShouldDeployPlugins( false ); break;
            case OptionArgumentTypeAdditionalLibrarySearchDirPaths:
            {
                const QString additionalLibrariesSearchDirPathsJoined = aParser->value( optionArgumentName );
                if (additionalLibrariesSearchDirPathsJoined.isEmpty())
                {
                    if ( errorText != nullptr )
                    {
                        *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                          QObject::tr( "Missing additional library search dir paths" ) );
                    }

                    return DeploymentSettings{};
                }

                static const QChar pathsSeparator(QLatin1Char(':'));
                const QStringList additionalLibrariesSearchDirPaths = additionalLibrariesSearchDirPathsJoined.split(pathsSeparator);
                for ( const QString& additionalLibrariesSearchDirPath : additionalLibrariesSearchDirPaths )
                {
                    const QDir dir(additionalLibrariesSearchDirPath);
                    if (! dir.exists())
                    {
                        if ( errorText != nullptr )
                        {
                            *errorText = DeploymentSettingsParser::errorText( aParser,
                                                                              QObject::tr( "One or more of the additional library search dir paths does not exist." ) );
                        }

                        return DeploymentSettings{};
                    }

                    additionalLibrarySearchPaths.append(dir.canonicalPath());
                }

                break;
            }
            default:
                break;
        }
    }

    deploymentSettings.setAdditionalExecutables( additionalExecutables );
    deploymentSettings.setAdditionalLibrarySearchDirPaths( additionalLibrarySearchPaths );
    if ( ! deploymentSettings.isValid() )
    {
        if ( errorText != nullptr )
        {
            *errorText = DeploymentSettingsParser::errorText( aParser );
        }

        return DeploymentSettings{};
    }

    return deploymentSettings;
}

QString DeploymentSettingsParser::errorText(
        QCommandLineParser* const aParser,
        const QString& aSpecificErrorMessage )
{
    if ( aParser == nullptr )
    {
        return QString{};
    }

    if ( aSpecificErrorMessage.isEmpty() )
    {
        return aParser->helpText();
    }

    const QString newline = Utilities::newline();
    return aSpecificErrorMessage
           % newline
           % QString{ aSpecificErrorMessage.size(), QLatin1Char( '-' ) }
           % newline
           % aParser->helpText();
}
