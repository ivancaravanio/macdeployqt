/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_LOGGER_H
#define MACDEPLOYQT_SHARED_LOGGER_H

#include "abstractloggerstream.h"

#include <QFile>
#include <QString>
#include <QTextStream>
#include <QWeakPointer>
#include <QSharedPointer>

namespace MacDeployQt {
namespace Shared {

enum SeverityLevel
{
    SeverityLevelNone    = 0x0,
    SeverityLevelError   = 0x1 << 1,
    SeverityLevelInfo    = 0x1 << 2,
    SeverityLevelWarning = 0x1 << 3,
    SeverityLevelDebug   = 0x1 << 4
};

Q_DECLARE_FLAGS(SeverityLevels, SeverityLevel)

enum VerbosityLevel
{
    VerbosityLevelNone = 0,
    VerbosityLevelErrors = 1,
    VerbosityLevelWarnings = 2,
    VerbosityLevelInfo = 3,
    VerbosityLevelDebug = 4
};

class Logger
{
private:
    Logger();
    Logger(const QString &filePath, const SeverityLevels &severityLevels);
    Logger(QIODevice *device, const SeverityLevels &severityLevels);
    Logger(FILE *fileHandle, const SeverityLevels &severityLevels);
    Logger(QString &string, const SeverityLevels &severityLevels);

public:
    ~Logger();

    static Logger& instance();

    void init(const QString &filePath, const SeverityLevels &severityLevels);
    void init(QIODevice *device, const SeverityLevels &severityLevels);
    void init(FILE *fileHandle, const SeverityLevels &severityLevels);
    void init(QString &string, const SeverityLevels &severityLevels);

    static bool isVerbosityLevelValid(const VerbosityLevel aVerbosityLevel);
    static bool isSeverityLevelValid(const SeverityLevel aSeverityLevel);
    static SeverityLevels verbosityLevelToSeverityLevels(const VerbosityLevel verbosityLevel);

    void setSeverityLevels(const SeverityLevels &severityLevels);

    AbstractLoggerStream &logError();
    AbstractLoggerStream &logWarning();
    AbstractLoggerStream &logInfo();
    AbstractLoggerStream &logDebug();

private:
    QString commonPrefix() const;
    QString commonPrefix(const SeverityLevel severityLevel) const;
    static QString severityLevelString(const SeverityLevel severityLevel);

private:
    class BufferStream : public AbstractLoggerStream
    {
    public:
        BufferStream();
        explicit BufferStream(QIODevice *device);
        explicit BufferStream(FILE *fileHandle, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        explicit BufferStream(QString *string, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        explicit BufferStream(QByteArray *array, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
        ~BufferStream() Q_DECL_OVERRIDE;

        AbstractLoggerStream &operator<<(QChar ch) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(char ch) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed short i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned short i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed int i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned int i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed long i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned long i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(qlonglong i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(qulonglong i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(float f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(double f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const QString &s) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QLatin1String s) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const QByteArray &array) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const char *c) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const void *ptr) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QTextStreamFunction f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QTextStreamManipulator m) Q_DECL_OVERRIDE;

        bool wasWrittenTo() const;

    private:
        Q_DISABLE_COPY(BufferStream)

    private:
        QTextStream m_textStream;
        bool m_wasWrittenTo;
    };

    class LineWriterStream : public AbstractLoggerStream
    {
    public:
        LineWriterStream();
        explicit LineWriterStream(const QWeakPointer< BufferStream > &bufferStream);
        ~LineWriterStream() Q_DECL_OVERRIDE;

        AbstractLoggerStream &operator<<(QChar ch) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(char ch) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed short i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned short i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed int i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned int i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(signed long i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(unsigned long i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(qlonglong i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(qulonglong i) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(float f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(double f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const QString &s) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QLatin1String s) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const QByteArray &array) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const char *c) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(const void *ptr) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QTextStreamFunction f) Q_DECL_OVERRIDE;
        AbstractLoggerStream &operator<<(QTextStreamManipulator m) Q_DECL_OVERRIDE;

        void setStream(const QWeakPointer<BufferStream> &bufferStream);

    private:
        Q_DISABLE_COPY(LineWriterStream)

    private:
        QWeakPointer<BufferStream> m_bufferStream;
    };

private:
    Q_DISABLE_COPY(Logger)

private:
    LineWriterStream m_errorStream;
    LineWriterStream m_warningStream;
    LineWriterStream m_infoStream;
    LineWriterStream m_debugStream;

    QSharedPointer<BufferStream> m_stream;
    QFile m_file;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(MacDeployQt::Shared::SeverityLevels)

}
}

#endif // MACDEPLOYQT_SHARED_LOGGER_H
