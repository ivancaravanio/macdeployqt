/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_DEPLOYER_H
#define MACDEPLOYQT_SHARED_DEPLOYER_H

#include "moduleinfo.h"

#include <QList>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QSet>

class tst_deployment_mac;

namespace MacDeployQt {
namespace Shared {

struct FrameworkInfo
{
    bool operator==(const FrameworkInfo &info) const;
    bool operator!=(const FrameworkInfo &info) const;

    QString description() const;

    bool    isDylib;
    QString frameworkDirectory;
    QString frameworkName;
    QString frameworkPath;
    QString binaryDirectory;
    QString binaryName;
    QString binaryPath;
    QString rpathUsed;
    QString version;
    QString installName;
    QString deployedInstallName;
    QString sourceFilePath;
    QString frameworkDestinationDirectory;
    QString binaryDestinationDirectory;
};

class Deployer
{
    friend class tst_deployment_mac;

private:
    Deployer();

public:
    static void changeQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QString &qtPath,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());

    static void deployQtFrameworks(
            const ModuleInfo&        aMainModuleInfo,
            const QList<ModuleInfo>& aAdditionalExecutables,
            const bool               aShouldUseDebugLibs,
            const bool               aShouldDeployPlugins,
            const bool               aShouldCreateDmg,
            const bool               aShouldStrip,
            const QString&           aQmlDir,
            const bool               aAlwaysOverwrite,
            const bool               aShouldCodeSign,
            const QString&           aCodeSignIdentity,
            const bool               aIsAppStoreCompliant,
            const QStringList&       aAdditionalLibraryPaths = QStringList());

private:
    struct DeploymentInfo
    {
        DeploymentInfo();

        QString qtPath;
        QString pluginPath;
        QStringList deployedFrameworks;
        QSet<QString> rpathsUsed;
        bool useLoaderPath;
    };

private:
    static const QString s_allCharactersAnyCountWildcard;

private:
    static void changeQtFrameworks(
            const QList<FrameworkInfo> &frameworks,
            const QList<ModuleInfo> &modules,
            const QString &qtPath);

    static bool copyFilePrintStatus(
            const QString &from,
            const QString &to,
            const bool alwaysOverwrite);
    static bool linkFilePrintStatus(const QString &file, const QString &link);
    static void patch_debugInInfoPlist(const QString &infoPlistPath);

    static FrameworkInfo parseOtoolLibraryLine(
            const ModuleInfo &mainModuleInfo,
            const QString &line,
            const QSet<QString> &rpaths,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QSet<QString> getBinaryRPaths(const QString &path, const bool resolve = true, const QString &executablePath = QString());
    static void deployRPaths(const ModuleInfo &mainModuleInfo, const QSet<QString> &rpaths, const QList<ModuleInfo> &dependencyModules, bool useLoaderPath);
    static void deployRPaths(const ModuleInfo &mainModuleInfo, const QSet<QString> &rpaths, const ModuleInfo &dependencyModule, bool useLoaderPath);
    static QString resolveDyldPrefix(const QString &path, const QString &loaderPath, const QString &executablePath);
    static QList<FrameworkInfo> getQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const ModuleInfo &moduleInfo,
            const QSet<QString> &rpaths,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QList<FrameworkInfo> getQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QStringList &otoolLines,
            const QSet<QString> &rpaths,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths = QStringList());
    static QList<FrameworkInfo> getQtFrameworksForPaths(
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &modules,
            const QSet<QString> &rpaths,
            const bool useDebugLibs,
            const QStringList &additionalLibraryPaths);
    static bool recursiveCopy(
            const QString &sourcePath,
            const QString &destinationPath,
            const bool alwaysOverwrite);
    static void recursiveCopyAndDeploy(
            const ModuleInfo &mainModuleInfo,
            const QSet<QString> &rpaths,
            const QString &sourcePath,
            const QString &destinationPath,
            const bool useDebugLibs,
            const bool useLoaderPath,
            const bool runStrip,
            const bool alwaysOverwrite,
            const QStringList &additionalLibraryPaths);
    static QString copyDylib(const FrameworkInfo& framework,
                             const QString& path,
                             const bool alwaysOverwrite);
    static QString copyFramework(
            const FrameworkInfo &framework,
            const ModuleInfo &mainModuleInfo,
            const bool alwaysOverwrite);
    static DeploymentInfo deployQtFrameworks(
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &additionalExecutables,
            const bool useDebugLibs,
            const bool runStrip,
            const bool alwaysOverwrite,
            const QStringList &additionalLibraryPaths = QStringList());
    static DeploymentInfo deployQtFrameworks(
            const QList<FrameworkInfo> &frameworks,
            const ModuleInfo &mainModuleInfo,
            const QList<ModuleInfo> &modules,
            const bool useDebugLibs,
            const bool useLoaderPath,
            const bool runStrip,
            const bool alwaysOverwrite,
            const QStringList &additionalLibraryPaths);
    static void createQtConf(const ModuleInfo &moduleInfo);
    static void deployPlugins(
            const ModuleInfo &mainModuleInfo,
            const QString &pluginSourcePath,
            const QString &pluginDestinationPath,
            const DeploymentInfo &deploymentInfo,
            const bool useDebugLibs,
            const bool runStrip,
            const bool alwaysOverwrite,
            const bool isAppStoreCompliant,
            const QStringList &additionalLibraryPaths);
    static void deployPlugins(
            const ModuleInfo &mainModuleInfo,
            const DeploymentInfo &deploymentInfo,
            const bool useDebugLibs,
            const bool runStrip,
            const bool alwaysOverwrite,
            const bool isAppStoreCompliant,
            const QStringList &additionalLibraryPaths = QStringList());
    static void deployQmlImport(
            const ModuleInfo &mainModuleInfo,
            const QSet<QString> &rpaths,
            const QString &importSourcePath,
            const QString &importName,
            const bool useDebugLibs,
            const bool useLoaderPath,
            const bool runStrip,
            const bool alwaysOverwrite,
            const QStringList &additionalLibraryPaths);
    static bool deployQmlImports(
            const ModuleInfo &mainModuleInfo,
            const DeploymentInfo &deploymentInfo,
            const QStringList &qmlDirs,
            const bool useDebugLibs,
            const bool runStrip,
            const bool alwaysOverwrite,
            const QStringList &additionalLibraryPaths);
    static void runInstallNameTool(const QStringList &options);
    static void changeIdentification(const QString &id, const QString &binaryPath);
    static void changeInstallName(
            const ModuleInfo &moduleInfo,
            const FrameworkInfo &framework,
            const QList<ModuleInfo> &modules,
            const bool useLoaderPath);
    static void changeInstallName(
            const QString &oldName,
            const QString &newName,
            const ModuleInfo &moduleInfo);
    static void runStrip(const QString &binaryPath);
    static void codesign(const ModuleInfo &moduleInfo, const QString &identity);
    static QSet<QString> codesignBundle(const ModuleInfo &moduleInfo, const QString &identity);
    static void codesignFile(const QString &identity, const QString &filePath);
    static void createDiskImage(const ModuleInfo &moduleInfo, const bool alwaysOverwrite);
    static QStringList getBinaryDependencies(const QString& executablePath, const QString &path);
    static QStringList findAppBundleFiles(const QString &appBundlePath);
    static void stripModuleBinary(const ModuleInfo &bundlePath);

    static bool syncExecuteTool( const QString& aToolName,
                                 const QStringList& aArgs,
                                 QString* const aOutput = nullptr,
                                 QString* const aErrorOutput = nullptr );
    static bool runOtool( const QStringList& aArgs,
                          QStringList* const aOutput,
                          QString* const aErrorOutput = nullptr );
    static bool binaryLoadCommands(
            const QString& aBinaryFilePath,
            QStringList* const aOutput,
            QString* const aErrorOutput = nullptr );
    static bool binarySharedLibraries(
            const QString& aBinaryFilePath,
            QStringList* const aOutput,
            QString* const aErrorOutput = nullptr );
};

}
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream &stream, const MacDeployQt::Shared::FrameworkInfo &info);

#endif // MACDEPLOYQT_SHARED_DEPLOYER_H
